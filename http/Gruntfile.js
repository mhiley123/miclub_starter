module.exports = function(grunt) {

    // Bootstrap grunt
    grunt.initConfig({

        // Read package file
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'cms/wp-content/themes/<%= pkg.name %>/assets/css/style.css' : 'cms/wp-content/themes/<%= pkg.name %>/assets/sass/style.scss'
                }
            },
            editor: {
                files: {
                    'cms/wp-content/themes/<%= pkg.name %>/editor.css' : 'cms/wp-content/themes/<%= pkg.name %>/assets/sass/style.scss'
                }
            },
            theme: {
                files: {
                    'cms/wp-content/themes/<%= pkg.name %>/style.css' : 'cms/wp-content/themes/<%= pkg.name %>/assets/sass/style.scss'
                }
            }
        },

        // Create uglify task and define parameters. Uglify consolidates and minifies JS.
        uglify: {
            options: {
                mangle: false,
                beautify: true
            },
            build: {
                files: {
                    'cms/wp-content/themes/<%= pkg.name %>/assets/js/scripts.min.js':
                        ['cms/wp-content/themes/<%= pkg.name %>/assets/js/*.js', '!cms/wp-content/themes/<%= pkg.name %>/assets/js/scripts.min.js']
                }
            }
        },

        // Ting PNG those bloaters
        tinypng: {
            options: {
                apiKey: "4I7zL9dm3vEx6hCBJUsJ2Slz7vO1ypSR",
                checkSigs: false,
                summarize: true,
                showProgress: true,
                stopOnImageError: true
            },
            uploads: {
                expand: true,
                cwd: 'cms/wp-content/uploads/',
                src: ['**/*.{png,jpg}'],
                dest: 'cms/wp-content/uploads/'
            },
            theme: {
                expand: true,
                cwd: 'cms/wp-content/themes/<%= pkg.name %>/assets/images/',
                src: ['**/*.{png,jpg}'],
                dest: 'cms/wp-content/themes/<%= pkg.name %>/assets/images/'
            }
        },

        'sftp-deploy': {
            sass: {
                auth: {
                    host: 'wei.miclub.internal',
                    port: 22,
                    authKey: 'credentials'
                },
                cache: 'sftpCache.json',
                src: 'cms/wp-content/themes/<%= pkg.name %>/assets/css/',
                dest: '/opt/miclub/main/golfsites/<%= pkg.server %>/http/cms/wp-content/themes/<%= pkg.name %>/assets/css/',
                exclusions: '.DS_Store',
                serverSep: '/',
                concurrency: 20,
                progress: true
            },
            js: {
                auth: {
                    host: 'wei.miclub.internal',
                    port: 22,
                    authKey: 'credentials'
                },
                cache: 'sftpCache.json',
                src: 'cms/wp-content/themes/<%= pkg.name %>/assets/js/',
                dest: '/opt/miclub/main/golfsites/<%= pkg.server %>/http/cms/wp-content/themes/<%= pkg.name %>/assets/js/',
                exclusions: '.DS_Store',
                serverSep: '/',
                concurrency: 20,
                progress: true
            },
            theme: {
                auth: {
                    host: 'wei.miclub.internal',
                    port: 22,
                    authKey: 'credentials'
                },
                cache: 'sftpCache.json',
                src: 'cms/wp-content/themes/<%= pkg.name %>/',
                dest: '/opt/miclub/main/golfsites/<%= pkg.server %>/http/cms/wp-content/themes/<%= pkg.name %>/',
                exclusions: '.DS_Store',
                serverSep: '/',
                concurrency: 20,
                progress: true
            }
        },

        // Setup watcher to do stuff all the time (this is just another task)
        // This will automatically execute the uglify and sass tasks when the specified files change
        watch: {
            css: {
                files: ['cms/wp-content/themes/<%= pkg.name %>/assets/sass/*.scss', 'cms/wp-content/themes/<%= pkg.name %>/assets/sass/*/*.scss'],
                tasks: ['sass:dist',  'sftp-deploy:sass']
            },
            js: {
                files: ['cms/wp-content/themes/<%= pkg.name %>/assets/js/*.js', '!cms/wp-content/themes/<%= pkg.name %>/assets/js/scripts.min.js'],
                tasks: ['uglify', 'sftp-deploy:js']
            },
            php: {
                files: ['cms/wp-content/themes/<%= pkg.name %>/*.php', 'cms/wp-content/themes/<%= pkg.name %>/*/*.php']
                //Simply run livereload.
            },
            options: {
                livereload: true
            }
        },

    });

    // Load task libs
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-tinypng');
    grunt.loadNpmTasks('grunt-sftp-deploy');

    // Begin watcher when 'grunt' is executed in bash/cmd prompt
    // the 'watch' task will minify js, convert sass to css etc
    grunt.registerTask('default', ['watch']);

    // Setup another task to only run stuff once, without monitoring
    grunt.registerTask('pass', ['sass','uglify']);

    // Speed the heck up
    grunt.registerTask('build', ['tinypng']);

    // Speed the heck up
    grunt.registerTask('editor', ['sass:editor', 'sftp-deploy:theme']);

    // Get prodin
    grunt.registerTask('prod', ['sass:dist',  'sftp-deploy:sass']);

}