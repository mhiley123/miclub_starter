<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gawler Golf Club &#8211; Sandy Creek</title>

    <link rel="shortcut icon" href="http://www.gawlergolf.com.au/cms/wp-content/themes/essential/favicon.ico"
          type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" media="all"
          href="http://www.gawlergolf.com.au/cms/wp-content/themes/essential/style.css"/>
    <link rel="stylesheet" type="text/css" media="all"
          href="http://www.gawlergolf.com.au/cms/wp-content/themes/essential/media.css"/>

    <script src="http://www.gawlergolf.com.au/cms/wp-includes/js/jquery/jquery.js?ver=1.10.2"></script>
    <script>window.gawlerJq = jQuery.noConflict();</script>
    <script src="http://www.gawlergolf.com.au/cms/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1"></script>

    <!-- START P4P -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
    <link href="/p4p/css/bootstrap.min.css" rel="stylesheet">
    <link href="/p4p/js/lib/bootstrap-datetimepicker-3.1.2/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="/p4p/js/lib/chosen_v1.1.0/chosen.min.css" rel="stylesheet">
    <link href="/p4p/css/chosen-bootstrap.css" rel="stylesheet">
    <link href="/p4p/css/classie-component.css" rel="stylesheet">
    <link href="/style/p4p2.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/p4p/js/lib/html5shiv-3.7.2.min.js"></script>
    <script src="/p4p/js/lib/respond-1.4.2.min.js"></script>
    <![endif]-->

    <script data-main="/p4p/js/app2" src="/p4p/js/require-2.1.14.min.js"></script>
    <!-- END P4P -->
</head>

<body class="home page page-id-4 page-template page-template-page-front-php">
<div id="wrapper">

    <div id="header" class="clearfix">
        <div id="header-top">
            <a href="http://www.gawlergolf.com.au/cms" class="logo">
                <img src="http://www.gawlergolf.com.au/cms/wp-content/themes/essential/images/logo.png"
                     alt="Gawler Golf Club &#8211; Sandy Creek"/>
            </a>

            <p class="address">
                <strong>Clubhouse:</strong> (08) 8524 4231 <br/>
                <strong>Proshop:</strong> (08) 8524 4315
            </p>
        </div>
        <div id="menu-toggle">
            <strong>Menu</strong>
        </div>
        <ul id="nav" class="guestsMenu">
            <li id="menu-item-60"
                class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item">
                <a href="http://www.gawlergolf.com.au/cms/">
                    <div class="left"></div>
                    <span>Home</span>

                    <div class="right"></div>
                </a></li>

            <ul class="sub-menu">
            </ul>

            <ul class="sub-menu">
            </ul>
            <li id="menu-item-61"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children arIco"><a
                    href="#">
                <div class="left"></div>
                <span>Golf</span>

                <div class="right"></div>
            </a>
                <ul class="sub-menu">
                    <li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/golf/visitors-and-groups/">
                        <div class="left"></div>
                        <span>Visitors and Groups</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/golf/proshop/">
                        <div class="left"></div>
                        <span>Proshop</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/golf/course-gallery/">
                        <div class="left"></div>
                        <span>Course Gallery</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-65" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/golf/local-rules-etiquette-dress-regulations/">
                        <div class="left"></div>
                        <span>Local Rules / Etiquette / Dress Regulations</span>

                        <div class="right"></div>
                    </a></li>
                </ul>
            </li>
            <li id="menu-item-77"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children arIco"><a
                    href="#">
                <div class="left"></div>
                <span>Membership</span>

                <div class="right"></div>
            </a>
                <ul class="sub-menu">
                    <li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/membership/information/">
                        <div class="left"></div>
                        <span>Information</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/membership/reciprocal-clubs/">
                        <div class="left"></div>
                        <span>Reciprocal Clubs</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/membership/enquiry-form/">
                        <div class="left"></div>
                        <span>Enquiry Form</span>

                        <div class="right"></div>
                    </a></li>
                </ul>
            </li>
            <li id="menu-item-81"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children arIco"><a
                    href="#">
                <div class="left"></div>
                <span>Hospitality &#038; Events</span>

                <div class="right"></div>
            </a>
                <ul class="sub-menu">
                    <li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/hospitality-events/functions/">
                        <div class="left"></div>
                        <span>Functions</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-83" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/hospitality-events/gallery/">
                        <div class="left"></div>
                        <span>Gallery</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-84" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/hospitality-events/enquiry-form/">
                        <div class="left"></div>
                        <span>Enquiry Form</span>

                        <div class="right"></div>
                    </a></li>
                </ul>
            </li>
            <li id="menu-item-85" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                    href="http://www.gawlergolf.com.au/cms/whats-on/">
                <div class="left"></div>
                <span>Whats On</span>

                <div class="right"></div>
            </a></li>
            <li id="menu-item-86"
                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children arIco"><a
                    href="#">
                <div class="left"></div>
                <span>About Us</span>

                <div class="right"></div>
            </a>
                <ul class="sub-menu">
                    <li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/about-us/contact-details/">
                        <div class="left"></div>
                        <span>Contact Details</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-88" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/about-us/location/">
                        <div class="left"></div>
                        <span>Location</span>

                        <div class="right"></div>
                    </a></li>
                    <li id="menu-item-89" class="menu-item menu-item-type-post_type menu-item-object-page"><a
                            href="http://www.gawlergolf.com.au/cms/about-us/history/">
                        <div class="left"></div>
                        <span>History</span>

                        <div class="right"></div>
                    </a></li>
                </ul>
            </li>
            <li id="menu-item-102" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="/members/">
                <div class="left"></div>
                <span>Members Login</span>

                <div class="right"></div>
            </a></li>
        </ul>
    </div>
    <!-- /#header -->

    <div id="main" class="clearfix">

        <c:choose>
            <c:when test="${not empty page}">
                <c:import url="${page}"/>
            </c:when>
            <c:otherwise>
                <c:import url="/jsp/p4p/landing.jsp"/>
            </c:otherwise>
        </c:choose>

    </div>
    <!-- /#main -->

    <div id="footer" class="clearfix">
        <p class="copyright">&copy; 2014 Gawler Golf Club. All rights reserved.</p> <span>|</span>

        <p class="contactus"><a href="/cms/about-us/contact-details/">Contact Us</a></p> <span>|</span>

        <p class="miclub"><a href="http://www.miclub.com.au/" target="_blank">Website by MiClub</a></p> <span>|</span>

        <p class="webaddress">WWW.GAWLERGOLF.COM.AU</p>
    </div>
    <!-- /#footer -->

</div><!-- /#wrapper -->

<script>
    !(function($) {
        $(function() {
            // Gawler Golf Club's Navigation
            $('#menu-toggle strong').click(function () {
                $('#menu-toggle strong').toggleClass("toggled-on");
                $('#nav').toggleClass("toggled-on");
            });
        });
    })(window.gawlerJq);
</script>
</body>
</html>