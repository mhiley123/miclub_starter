<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Make Bookings</title>
<!-- InstanceEndEditable -->
<%@ include file="/msp/headM.jsp"%>
<!-- InstanceBeginEditable name="Body" -->
						<miclub:message key=""/>
						
						  <p align="center">Thank you for registering as a public member. You should shortly be receiving your username and password which has be emailed to the account you provided.</p>
                          <p align="center">Once you have received your username and password click <a href="/security/login.msp">here</a> to log on. </p>
                        <!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->