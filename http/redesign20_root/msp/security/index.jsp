<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- InstanceBegin template="/Templates/PrivateClub-Guests.dwt" codeOutsideHTMLIsLocked="false" -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Members Section</title>
<!-- InstanceEndEditable -->
<c:import url="/msp/headG.jsp"/>
<!-- InstanceBeginEditable name="Body" -->
<table width="700" height="500" border="0" align="center" cellpadding="5" cellspacing="0" class="static">
<tr>
<td colspan="2">
<h1><miclub:message key="TITLE_LOGIN"/></h1></td>
</tr>
<tr>
<td colspan="2"><div align="center"><c:if test="${error}"><miclub:import url="/jsp/templates/GUIerror.jsp"> <miclub:param name="error" value="ERROR_LOGIN"/> </miclub:import></c:if> </div></td>
</tr>
<tr>
<td width="313" valign="top"><div>
<form name="form" method="post" action="/security/login.msp">
<div align="left"></div>
<table width="300" class="portal" border="0" cellpadding="4" cellspacing="0">
<tr>
<td width="48%"><div align="right">                   <miclub:message key="FIELD_USERNAME"/></div></td>
<td width="52%"><div align="left">
<input name="action" type="hidden" id="action" value="login">
<miclub:import url="/jsp/templates/textfield.jsp">
<miclub:param name="name">user</miclub:param>
<miclub:param name="value"> </miclub:param>
<miclub:param value="11" name="size"/>
<miclub:param name="maxlength" value="32"/>
</miclub:import>
</div></td></tr>
<tr>
<td><div align="right"><miclub:message key="FIELD_PASSWORD"/></div></td>
<td><div align="left">
<miclub:import url="/jsp/templates/textfield.jsp">
<miclub:param name="name">password</miclub:param>
<miclub:param name="value"> </miclub:param>
<miclub:param value="11" name="size"/>
<miclub:param value="true" name="password"/>
<miclub:param name="maxlength" value="64"/>
</miclub:import>
</div></td>
</tr>
<tr>
<td></td>
<td><div align="left">
<input type="submit" name="Submit" value="<miclub:message key="BUTTON_LOGIN"/>"></div></td>
</tr>
<tr>
<td colspan="2"> <div align="center"><a href="/security/passwordReminder.msp"><miclub:message key="LINK_PASSWORD_FINDER"/></a>
</div></td>
</tr>
<tr>
<td colspan="2"><div align="center"><a href="javascript:MM_openNewWindow('/guests/static/help/index.msp','noName',520,400,'true');">Need help logging on?</a></div></td>
</tr>
</table>
</form>
</div></td>
<td width="367" valign="top"><miclub:message key="TEXT_MEMBERS_ONLY"/><br/>
<miclub:message key="TEXT_LOGIN_NOTICE"/><br/>
<miclub:message key="TEXT_TERMS_AND_CONDITIONS"/>
</td>
</tr>
</table>
<script language="JavaScript">
document.forms['form'].elements['user'].focus();
</script>
<!-- InstanceEndEditable -->
<c:import url="/msp/footQ.jsp"/><!-- InstanceEnd -->