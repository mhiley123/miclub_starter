<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- InstanceBegin template="/Templates/PrivateClub-Guests.dwt" codeOutsideHTMLIsLocked="false" --><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Members Section</title>
<script type="text/javascript" src="/cms/memlogout.php"></script>
<!-- InstanceEndEditable -->
<%@ include file="/msp/headG.jsp"%>
<!-- InstanceBeginEditable name="Body" -->
<h1>Login</h1>
<div id="login-left">
<form action="/security/login.msp" method="post" name="form" id="form">
<label>Your Username</label>
<input name="action" type="hidden" id="action" value="login" /><input autocomplete="on" type="text" name="user" value="" size="11" maxlength="32" />
<label>Password</label>
<input autocomplete="on" type="password" name="password" value="" size="11" maxlength="64" />
<input type="submit" name="Submit" value="Login" id="submit" />
<p>
<a href="/security/passwordReminder.msp">Forgotten your password?</a><br />
<a href="javascript:MM_openNewWindow('/guests/static/help/index.msp','noName',520,400,'true');">Need help logging on?</a>
</p>
</form>
</div>						
<div id="login-right">
<div id="login-error">
<c:import url="/jsp/templates/pageError.jsp"/>
<c:if test="${error}"><c:import url="/jsp/templates/GUIerror.jsp"><c:param name="error" value="ERROR_LOGIN"/></c:import></c:if>
</div>
<miclub:message key="TEXT_MEMBERS_ONLY"/>
<miclub:message key="TEXT_LOGIN_NOTICE"/>
<miclub:message key="TEXT_TERMS_AND_CONDITIONS"/>
</div>
<script language="JavaScript" type="text/javascript">
document.forms['form'].elements['user'].focus();
</script>
<!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->