<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- InstanceBegin template="/Templates/PrivateClub-Guests.dwt" codeOutsideHTMLIsLocked="false" --><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Members Section</title>
<!-- InstanceEndEditable -->
<%@ include file="/msp/headG.jsp"%>
<!-- InstanceBeginEditable name="Body" -->
<h1>Password Reminder</h1>
<div id="password-reminder">
<p><c:choose><c:when test="${error}"><miclub:message key="TEXT_PASSWORD_FINDER_FAILED_NO_EMAIL"/></c:when><c:when test="${errorId}"><miclub:message key="TEXT_PASSWORD_FINDER_FAILED_BAD_MEMBERSHIP_NUMBER"/></c:when></c:choose></p>
<p><c:choose>
<c:when test="${error or errorId}">
<miclub:message key="TEXT_PASSWORD_RETRIEVE_RETRY"/>
</c:when>
<c:when test="${!success}">
<miclub:message key="TEXT_PASSWORD_FINDER_INSTRUCTIONS"/>
</c:when>
<c:otherwise>
<miclub:message key="TEXT_PASSWORD_FINDER_SUCCEEDED" parameters="<%=(List) request.getAttribute("emailList")%>"/>							
</c:otherwise>						
</c:choose></p>
<c:if test="${!success}">
<form name="form" method="post" action="/security/passwordReminder.msp">
<label><miclub:message key="FIELD_MEMBERSHIP_NUMBER"/></label>
<input name="membershipNumber" type="text" id="membershipNumber">
<input type="submit" name="Submit" id="submit" value="<miclub:message key="BUTTON_CONTINUE"/>">
</form>
</c:if>
</div>
<!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->