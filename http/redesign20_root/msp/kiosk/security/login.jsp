<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd"><html><head><!-- InstanceBegin template="/Templates/PrivateClub-Kiosk.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Private Club : Kiosk</title>
<link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css">
<link href="/style/productStyle/privateClubKiosk.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/scripts/miclub.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body>
	<!-- InstanceBeginEditable name="Body" -->
	<script type="text/javascript">
	
	function removeCharacter(formObject) {	
		loginString = formObject.value;
		loginString = loginString.replace("09999","");
		formObject.value=loginString;
	}
	
	</script>
    <table width="500" border="0" align="center" cellpadding="5" cellspacing="0">
      <tr>
        <td><h1 align="center"><miclub:message key="TITLE_LOGIN"/></h1></td>
      </tr>
      <tr>
        <td><div align="center"><miclub:message key="TEXT_MEMBERS_ONLY"/> </div></td>
      </tr>
      <tr>
        <td><div align="center"><miclub:message key="KIOSK_TEXT_LOGIN_NOTICE"/></div></td>
      </tr>	  
      <tr>
        <td><div align="center"><c:if test="${error}"><c:import url="/jsp/templates/GUIerror.jsp"> <c:param name="error" value="ERROR_LOGIN"/> </c:import></c:if> </div></td>
      </tr>
      <tr>
        <td><div>
            <form name="form" method="post" action="/security/login.msp">
              <div align="center">
                <table  border="0" align="center" cellpadding="0" cellspacing="5">
                  <tr>
                    <td width="34%"><div align="right"><miclub:message key="FIELD_USERNAME"/></div></td>
                    <td width="66%"><div align="left">
                      <input class="text" name="user" type="text" id="user" onChange="javascript:removeCharacter(this)">
                        <input name="action" type="hidden" id="action" value="login">
                    </div></td>
                  </tr>

           		<c:if test="${showPassword}">
                  <tr>
                    <td><div align="right"><miclub:message key="FIELD_PASSWORD"/></div></td>
                    <td><div align="left">
                      <input class="text" name="password" type="password" id="password">
                    </div></td>
                  </tr>
           		</c:if>
           		
                  <tr>
                    <td>&nbsp;</td>
                    <td><div align="left">
                      <input class="button "type="submit" value="<miclub:message key="BUTTON_LOGIN"/>"/></div></td>
                  </tr>
                </table>
              </div>             
            </form>
        </div></td>
      </tr>
    </table>
	<script language="JavaScript">
		document.forms['form'].elements['user'].focus();
	</script>
  <!-- InstanceEndEditable --></body>
<!-- InstanceEnd --></html>
<%out.flush();%>

