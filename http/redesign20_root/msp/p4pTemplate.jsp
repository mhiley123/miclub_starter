<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<!-- InstanceBegin template="/Templates/PrivateClub-Guests.dwt" codeOutsideHTMLIsLocked="false" -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <!-- InstanceBeginEditable name="pageTitle" -->
    <title>Private Club : Members Section</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
    <link href="/p4p/css/bootstrap.min.css" rel="stylesheet">
    <link href="/p4p/js/lib/bootstrap-datetimepicker-3.1.2/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="/p4p/js/lib/chosen_v1.1.0/chosen.min.css" rel="stylesheet">
    <link href="/p4p/css/chosen-bootstrap.css" rel="stylesheet">
    <link href="/p4p/css/classie-component.css" rel="stylesheet">
    <link href="/style/p4p.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/p4p/js/lib/html5shiv-3.7.2.min.js"></script>
    <script src="/p4p/js/lib/respond-1.4.2.min.js"></script>
    <![endif]-->

    <script data-main="/p4p/js/app" src="/p4p/js/require-2.1.14.min.js"></script>
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
            <img src="/images/exampleHeader.jpg" width="100%" />
        </div>
    </div>
    <div class="row" style="padding: 10px">
        <div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-4 col-xs-4">
            <FORM NAME="URLlist">
                <td class="members_bodyprefix"><SELECT NAME="droplist" OnChange="location.href=URLlist.droplist.options[selectedIndex].value">
                    <OPTION SELECTED="SELECTED" VALUE=" ">Select a Destination...</OPTION>
                    <OPTION VALUE="/members/bookings/index.msp?booking_resource_id=3000000">Golf Bookings</OPTION>
                    <OPTION VALUE="/members/bookings/index.msp?booking_resource_id=3000001">Pro Bookings</OPTION>
                    <OPTION VALUE="/members/bookings/index.msp?booking_resource_id=3000003">Restaurant Bookings</OPTION>
                    <OPTION VALUE="/members/accounts/DisplayAccount.msp">Accounts</OPTION>
                    <OPTION VALUE="/members/directory/index.msp">Members Directory</OPTION>
                    <OPTION VALUE="/members/business-listings/index.xsp">Business Listings</OPTION>
                    <OPTION VALUE="/members/scorecard/index.msp">MiScore</OPTION>
                    <OPTION VALUE="/members/bulletin-boards/DisplayBulletinBoard.msp">Bulletin Boards</OPTION>
                    <option value="/members/bookings/functions/index.msp"><miclub:message key="ADMIN_BOOKING_FUNCTION"/></option>
                </SELECT>
                </td>
            </FORM>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-8" style="text-align: right;">
            | <a href="/members/index.msp">Your home </a>
            | <a href="/members/details/password/change-password.xsp">Change Password</a>
            | <a href="/security/logout.action.xsp">Logout</a>
            |
        </div>
    </div>

    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">

            <div id="slide-nav" class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="navbar-header">
                                <a class="navbar-toggle">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </a>
                                <a class="navbar-brand" href="http://www.miclub.com.au">
                                    Tee Bookings
                                </a>
                            </div>
                            <!-- /.navbar-header -->

                            <div id="slidemenu">

                                <!-- Search -->
                                <!--<form id="form-search" class="navbar-form navbar-right" role="form">-->
                                <!--<div class="form-group">-->
                                <!--<input type="search" placeholder="search" class="form-control">-->
                                <!--</div>-->
                                <!--<button type="submit" class="btn btn-primary">Search</button>-->
                                <!--</form>-->

                                <ul class="nav navbar-nav">
                                    <li><a href="/members/index.msp">Home</a></li>
                                    <li class="active"><a href="#">Find A Time</a></li>
                                    <li><a href="#about">Player Details</a></li>
                                    <li><a href="#contact">Payment</a></li>
                                    <li><a href="#confirmation">Confirmation</a></li>

                                    <!--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact <b-->
                                    <!--class="caret"></b></a>-->
                                    <!--<ul class="dropdown-menu">-->
                                    <!--<li><a href="#">Action</a></li>-->
                                    <!--<li><a href="#">Another action</a></li>-->
                                    <!--<li><a href="#">Something else here</a></li>-->
                                    <!--</ul>-->
                                    <!--</li>-->
                                </ul>
                                <!-- /.nav navbar-nav -->
                            </div>
                            <!-- /#slidemenu -->
                        </div>
                    </div>
                </div>
            </div><!-- /#slide-nav -->

            <!-- InstanceBeginEditable name="Body" -->
            <c:choose>
                <c:when test="${not empty page}">
                    <c:import url="${page}"/>
                </c:when>
                <c:otherwise>
                    <c:import url="/jsp/p4p/landing.jsp" />
                </c:otherwise>
            </c:choose>
            <!-- InstanceEndEditable -->

        </div>
    </div>

    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12">
            <img src="/images/examplePageFooter.jpg" width="100%" />
        </div>
    </div>
</div>

</body>

<!-- InstanceEnd -->
</html>
