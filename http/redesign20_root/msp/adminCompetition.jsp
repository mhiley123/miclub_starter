<%@ taglib prefix="miclub" uri="/miclub"%>
<%@ taglib prefix="cache" uri="http://www.opensymphony.com/oscache" %>
<%@ page import="au.com.miclub.framework.servlet.actions.MspView"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title><%=request.getAttribute(MspView.ATTR_TITLE) %></title>
<!-- miClub Javascript Library... do not edit or remove -->
<script type="text/javascript" src="/scripts/miclub.js"></script>
<!-- miClub product stylesheet... do not edit or remove -->
<link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css"> 
<!-- customise your own style sheet for this site -->
<link href="/style/custom.css" rel="stylesheet" type="text/css"> 

<link rel="stylesheet" href="/style/productStyle/adminCompetitionSheet.css" type="text/css"></link>

<script type="text/javascript" src="/scripts/prototype.js"></script>
<script type="text/javascript" src="/scripts/accordion2.js"></script>
<script type="text/javascript" src="/scripts/scriptaculous.js"></script>
<script type="text/javascript" src="/scripts/effects.js"></script>
<script type="text/javascript" src="/scripts/dragdrop.js"></script>

</head>

<body>
     
<div id="npContainer">
<jsp:include page="/jsp/golf/competition/adminHeader.jsp"/>
<div id="npBody" align="center">
<table cellspacing="0" cellpadding="1" border="0" width="710">     
<tr><td align="left">         
<!-- InstanceBeginEditable name="Body" -->
<miclub:import url="<%=(String)request.getAttribute(MspView.ATTR_PAGE) %>" />
<!-- InstanceEndEditable -->
</td></tr>
</table>
</div>
</div>

</body>
<!-- InstanceEnd -->
</html>

<%out.flush();%>
