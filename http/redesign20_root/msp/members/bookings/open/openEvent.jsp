<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd"><html><head><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Booking Event</title>
<!-- InstanceEndEditable -->
<!-- miClub Javascript Library... do not edit or remove -->
<script type="text/javascript" src="/scripts/miclub.js"></script>
<!-- miClub product stylesheet... do not edit or remove -->
<link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css"> 
<!-- customise your own style sheet for this site -->
<link href="/style/custom.css" rel="stylesheet" type="text/css"> 
</head>
<body>

<c:if test="${isUserResourceAdmin}">
 <!-- InstanceBeginEditable name="Body" --><c:import url="/jsp/bookings/open/openEvent.jsp"/><!-- InstanceEndEditable -->
</c:if>

<c:if test="${not isUserResourceAdmin}">
  <table width="750" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table border="0" align="center" cellpadding="0" cellspacing="0" class="bodyprefix">
          <tr class="bodyprefix" height="50">
            <td class="bodyprefix"><img src="/images/exampleHeader.jpg" width="750" height="100"/></td>
          </tr>
          <tr>
            <td height="24" valign="top" background="/images/exampleBackground.gif" class="bodyprefix"><table width="750" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="100%" cellpadding="4" cellspacing="0" height="36">
                      <tr valign="middle">
                        <FORM NAME="URLlist">
                          <td class="members_bodyprefix"><SELECT NAME="droplist" OnChange="location.href=URLlist.droplist.options[selectedIndex].value">
                              <OPTION SELECTED="SELECTED" VALUE=" ">Select a Destination...</OPTION>
                              <OPTION VALUE="/members/bookings/index.msp?booking_resource_id=3000000">Golf Bookings</OPTION>
                              <OPTION VALUE="/members/bookings/index.msp?booking_resource_id=3000001">Pro Bookings</OPTION>
                              <OPTION VALUE="/members/bookings/index.msp?booking_resource_id=3000003">Restaurant Bookings</OPTION>
                              <OPTION VALUE="/members/accounts/DisplayAccount.msp">Accounts</OPTION>
                              <OPTION VALUE="/members/directory/index.msp">Members Directory</OPTION>
                              <OPTION VALUE="/members/business-listings/index.xsp">Business Listings</OPTION>
                              <OPTION VALUE="/members/scorecard/index.msp">MiScore</OPTION>
                              <OPTION VALUE="/members/bulletin-boards/DisplayBulletinBoard.msp">Bulletin Boards</OPTION>
                              <option value="/members/bookings/functions/index.msp"><miclub:message key="ADMIN_BOOKING_FUNCTION"/></option>
                            </SELECT>
                          </td>
                        </FORM>
                        <td class="members_bodyprefix"> | <a href="/members/index.msp">Your home </a> | <a href="/members/details/password/change-password.xsp">Change Passord</a> | <a href="/security/logout.action.xsp">Logout</a> | </td>
                      </tr>
                    </table></td>
                </tr>
                <tr>
                  <td><table width="700"  border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td><!-- InstanceBeginEditable name="Body" --><c:import url="/jsp/bookings/open/openEvent.jsp"/><!-- InstanceEndEditable --></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td class="bodyprefix" valign="top" height="24"><img src="/images/examplePageFooter.jpg" width="750" height="32"/></td>
          </tr>
        </table></td>
    </tr>
  </table>
</c:if>
</body>
<!-- InstanceEnd --></html>
<%out.flush();%>
