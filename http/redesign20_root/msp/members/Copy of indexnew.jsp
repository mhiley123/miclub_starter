<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>MiClub Test Site: Members Section</title>
<miclub:property-is-true key="wordpress.enabled">
<c:set scope="request" var="login_url"><miclub:property key="wordpress.login.url"/>?name=<c:out value='${user_membership_number}' /></c:set>
<script type="text/javascript" src="<c:out value='${login_url}' />"></script>
</miclub:property-is-true>

<script src="/cms/wp-includes/js/jquery/jquery.js?ver=1.7.1" type="text/javascript"></script>
<link href="/cms/wp-content/themes/essential/css/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" media="screen" />
<script src="/cms/wp-content/themes/essential/js/jquery-tabsAccordian.js" type="text/javascript"></script>
<script src="/cms/wp-content/themes/essential/js/TAscript.js" type="text/javascript"></script>

<script type="text/javascript">
jQuery(document).ready(function() {
jQuery("#accordion").accordion({autoHeight:false,header:"h3.accHead",active:0});
jQuery('#tabcontainer').tabs({ fx:{opacity:'toggle' } });
jQuery("#two").click(function () { 
jQuery("#tabcontainer").tabs('select',1);});
jQuery("#three").click(function () { 
jQuery("#tabcontainer").tabs('select',2);});
});
</script> 

<!-- InstanceEndEditable -->
<c:import url="/msp/headM.jsp"/>
<!-- InstanceBeginEditable name="Body" -->  <c:import url="/jsp/portal/administrator.jsp"/>
<div id="leftBodyM">



      <div id="accordion">
 
  <div>
  <h3 class="accHead"><a href="#">My Bookings</a></h3>
  <div>
  <c:import url="/members/portal.myBookings.msp"/>
  </div>
  </div>
  
  <div>
   <h3 class="accHead"><a href="#">My Results</a></h3>
  <div>
  <c:import url="/members/portal.myResults.msp"/>
  </div>
  </div>
  
  <div>
   <h3 class="accHead"><a href="#">Webcam</a></h3>
  <div><c:import url="/members/CourseConditions?doAction=portlet"/>
  </div>
  </div>
  
 
  
  <div>
 <h3 class="accHead"><a href="#">Playing Partners</a></h3>
  <div>
  <c:import url="/members/portal.myPartners.msp"/>
  </div>
  </div>
  
  <div>
   <h3 class="accHead"><a href="#">My Details</a></h3>
  <div>
  <c:import url="/members/portal.myDetails.msp"/>
  </div>
  </div>
  
  <div>
 <h3 class="accHead"><a href="#">Poll</a></h3>
  <div>
  <c:import url="/members/Poll?doAction=portlet&location=1"/>
    <c:import url="/members/Poll?doAction=portlet&location=2"/>
  </div>
  </div>
  
  
    <div>
 <h3 class="accHead"><a href="#">My Prizes</a></h3>
  <div>
 <c:import url="/members/bookings/prize.account.msp"/>
  </div>
  </div>
  
  
    <div>
 <h3 class="accHead"><a href="#">Leaderboards</a></h3>
  <div>
<c:import url="/members/portal.competition.msp"/>
  </div>
  </div>
      <div>
 <h3 class="accHead"><a href="#">Support Our Sponsors</a></h3>
  <div>
 <c:import url="/generics/DisplayRandomGenericItem?generic_item_config_id=999990&page=/jsp/members/generics/businessListings/banner.jsp"/> 
  </div>
  </div>
 
  
    <div>
 <h3 class="accHead"><a href="#">Accounts</a></h3>
  <div>
<miclub:import url="/members/bookings/ball.account.msp"/>
  </div>
  </div>
</div>
</div>
<div id="rightBodyM">
 <div id="date"><script language = "javascript" type="text/javascript">
<!--
today = new Date
weekDayName = new Array ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
monthName = new Array ("January","February","March","April","May","June","July","August","September","October","November","December")
function printDate()

{
document.write("<b>" + weekDayName[today.getDay()]+ ", " + monthName[today.getMonth()] + " " + today.getDate()+ "</b>")
}
--></script><SCRIPT LANGUAGE="JAVASCRIPT">printDate()</SCRIPT></div>
 <h1>Welcome  <c:out value="${firstName}" /></h1>
<div id="tabcontainer">
            <ul>
                <li><a href="#HeadlineNews"><span>News</span></a></li>
                <li><a href="#ClubNews"><span>Club</span></a></li>
                <li><a href="#GolfNews"><span>Golf</span></a></li>
     
                <li><a href="#LadiesNews"><span>Ladies</span></a></li>
            </ul>
            <div id="HeadlineNews">
        <c:import url="/jsp/templates/noticeBoard.jsp"><c:param name="name" value="announcements"/></c:import></div>
            <div id="ClubNews">
                <c:import url="/jsp/templates/noticeBoard.jsp"><c:param name="name" value="cllubnews"/></c:import>
            </div>

            <div id="GolfNews">
                 <c:import url="/jsp/templates/noticeBoard.jsp"><c:param name="name" value="golfnews"/></c:import>
            </div>
          
            <div id="LadiesNews">
                 <c:import url="/jsp/templates/noticeBoard.jsp"><c:param name="name" value="ladies"/></c:import>
            </div>
        </div>
</div>

<!-- InstanceEndEditable -->
<c:import url="/msp/footQ.jsp"/><!-- InstanceEnd -->