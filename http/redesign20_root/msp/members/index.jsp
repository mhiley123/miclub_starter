<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" --><head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Members Section</title>
<!-- InstanceEndEditable -->
<%@ include file="/msp/headM.jsp"%>
<!-- InstanceBeginEditable name="Body" --> 
                        <c:import url="/jsp/portal/administrator.jsp"/>
<table width="710" border="0" align="center" cellpadding="3" cellspacing="3">
    <tr>
      <td colspan="2"><h1>Welcome <c:out value="${greetingName}" /></h1><table width="100%" border="0" cellspacing="0" cellpadding="5"  class="weather">
        <tr>
        <script>
                weatherWidth="100%";
                weatherCellpadding="2";
                weatherCellspacing="1";
                weatherBgcolor="#ffffff";
                weatherBordercolor="#ffffff";
                weatherBorder="0";
                weatherBordercolorlight="#ffffff";
                weatherBordercolordark="#ffffff";
           </script>
  
	<td valign="top">
	<miclub:property-is-not-true key="courseConditions.trial.enabled">
		<miclub:property-is-true key="courseConditions.enabled">
	 <miclub:import url="/members/CourseConditions?doAction=banner"/>
		</miclub:property-is-true>
	</miclub:property-is-not-true>
<!--script type="text/javascript" src="http://data.theweather.com.au/access/forecast4_horiz.jsp?iconsize=30&amp;location=94765"></script--></td>
                </tr>
              </table></td>
    </tr>
    <tr>
      <td width="230" valign="top"><table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
      <%--
        <tr>
          <td valign="top" width="100%">
          	<table class="portal" width="100%">
          		<tr>
          			<th>Support our sponsors!</th>
          		</tr>
          		<tr>
          			<td width="100%">
       			          <c:import url="/generics/DisplayRandomGenericItem?generic_item_config_id=999990&page=/jsp/members/generics/businessListings/banner.jsp"/>
          			</td>
       			</tr>
   			</table>
          </td>
        </tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>
		--%>
<miclub:property-is-true key="news.light.enabled">
        <tr>
          <td valign="top">
          <!-- style type="text/css">

/*Example CSS for the two demo scrollers*/

#pscroller1{
width: 220px;
height: 100px;
border: 1px solid black;
padding: 5px;
background-color: lightyellow;
}

.someclass{ //class to apply to your scroller(s) if desired
}
</style -->

<script type="text/javascript">
var announceContent=new Array();
announceContent[0]='<c:import url="/jsp/templates/noticeAnnounce.jsp"><c:param name="name" value="portal.news.1" /></c:import>';
announceContent[1]='<c:import url="/jsp/templates/noticeAnnounce.jsp"><c:param name="name" value="portal.news.2" /></c:import>';
announceContent[2]='<c:import url="/jsp/templates/noticeAnnounce.jsp"><c:param name="name" value="portal.news.3" /></c:import>';

//new pausescroller(name_of_message_array, CSS_ID, CSS_classname, pause_in_miliseconds)
new pausescroller(announceContent, "pscroller1", "portal", 3000)
</script>


			</td>
        </tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>		
</miclub:property-is-true>

        <tr>
          <td valign="top"><c:import url="/members/portal.myDetails.msp"/></td>
        </tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>		
        <tr>
          <td><c:import url="/members/portal.myaccount.msp"/></td>
        </tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>
	<miclub:property-is-not-true key="courseConditions.trial.enabled">
		<miclub:property-is-true key="courseConditions.enabled">
		        <tr>
		          <td valign="top"><c:import url="/members/CourseConditions?doAction=portlet"/></td>
		        </tr>
				<tr>
				  <td>&nbsp;</td>
				</tr>		
		</miclub:property-is-true>
	</miclub:property-is-not-true>
        <tr>
          <td valign="top" height="0"><c:import url="/members/portal.myBookings.msp"/></td>
        </tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>		
        <tr>
          <td valign="top" height="0"><c:import url="/members/portal.myResults.msp"/></td>
        </tr>
    <miclub:property-is-true key="competition.golf.leaderboard.enabled">
		<tr>
		  <td>&nbsp;</td>
		</tr>		
        <tr>
          <td valign="top" height="0"><c:import url="/members/portal.competition.msp"/></td>
        </tr>
    </miclub:property-is-true>
		<tr>
		  <td>&nbsp;</td>
		</tr>		
        <tr>
          <td valign="top" height="0"><c:import url="/members/portal.myPartners.msp"/></td>
        </tr>
      </table></td>
      <td rowspan="3" valign="top"><table width="460"  border="0" align="right" cellpadding="0" cellspacing="0" class="portal">
        <tr>
          <th>News</th>
        </tr>
        <tr>
          <td><c:import url="/members/portal.noticeboard.msp"/></td>
        </tr>
      </table>
      </td>
    </tr>
  </table>

<!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->