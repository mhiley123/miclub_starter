<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Members Section</title>
<miclub:property-is-true key="wordpress.enabled">
<c:set scope="request" var="login_url"><miclub:property key="wordpress.login.url"/>?name=<c:out value='${user_membership_number}' /></c:set>
<script type="text/javascript" src="<c:out value='${login_url}' />"></script>
</miclub:property-is-true>
<script type="text/javascript" src="/scripts/jquery.js"></script>
<script type="text/javascript" src="/scripts/jquery-tabsAccordian.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/cms/wp-content/themes/essential/jquery-ui-1.8.23.custom.css" />
<script type="text/javascript">
jQuery(document).ready(function($) {
$("#accordion").accordion({autoHeight:false,header:"h3.accHead",active:1});
$('#tabcontainer').tabs({ fx:{opacity:'toggle' } });
});
</script>
<script type="text/javascript">
function parseJsonp(xlmp, limit, url)
{
	$(xlmp).html('<h3>Loading...</h3>');
	$.ajax({
		type:'GET',
		url:url + '?json=1&callback=feedCallback&include=url,title,thumbnail,excerpt,date',
		data:"date_format=d F Y | H:i",
		success:function(feed) {
			var txtArray = [];
			for(var i=0, l=feed.posts.length; i < l && i < limit; ++i) 
			{
				var title = '<div class="news-item"><div class="post-meta"><h2 class="post-title"><a href=' + feed.posts[i].url + ' class="titleMore">' + feed.posts[i].title + '</a></h2><small class="post-data">Posted on '+ feed.posts[i].date + '</small></div>';
				var description = '<div class="post-content">' + feed.posts[i].excerpt + '</div></div>';
				txtArray.push(title,description);
			}
			$(xlmp).html(txtArray.join(''));
		},
		dataType:'jsonp'
	});
}

$(document).ready(function() {
	parseJsonp('#mainFeed1', 9, '/cms/category/club-news/');
	$('#two').click(function() {
	parseJsonp('#mainFeed2', 9, '/cms/category/mens-golf/');
    });
    $('#three').click(function() {
	parseJsonp('#mainFeed3', 9, '/cms/category/ladies-golf/');
    });
	 $('#four').click(function() {
	parseJsonp('#mainFeed4', 9, '/cms/category/club-knockouts/');
    });
});
</script><!-- InstanceEndEditable -->
<%@ include file="/msp/headM.jsp"%>
<!-- InstanceBeginEditable name="Body" -->
<script type="text/javascript" src="/scripts/jquery-tabsAccordian.js"></script>
<h1 id="welcome">Welcome <span class="name"><c:out value="${greetingName}" /></span>,</h1>

<c:import url="/jsp/portal/administrator.jsp"/>

<span id="date">
	<script type="text/javascript">
		<!--
		today = new Date
		weekDayName = new Array ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
		monthName = new Array ("January","February","March","April","May","June","July","August","September","October","November","December")
		function printDate()
		{
		document.write(weekDayName[today.getDay()]+ ", " + monthName[today.getMonth()] + " " + today.getDate())
		}
		-->
	</script>
	<script type="text/javascript">printDate()</script>
</span>

<div id="portal-main">

	<div id="tabcontainer">
    
		<ul>
            <li><a href="#mainFeed1"><span>Club News</span></a></li>
            <li><a href="#mainFeed2" id="two"><span>Men's Golf</span></a></li>
            <li><a href="#mainFeed3" id="three"><span>Ladies' Golf</span></a></li>
            <li><a href="#mainFeed4" id="four"><span>Club Knockouts</span></a></li>
		</ul>
        
		<div id="tabitems">
            <div id="mainFeed1" class="tab-content"></div>
            <div id="mainFeed2" class="tab-content"></div>
            <div id="mainFeed3" class="tab-content"></div>
            <div id="mainFeed4" class="tab-content"></div>
		</div>      
        
	</div>
	
</div>

<div id="accordion">

	<div>
		<h3 class="accHead"><a href="#">My Details</a></h3>
		<div>
		<c:import url="/members/portal.myDetails.msp"/>
		</div>
	</div>
	
	<div>
		<h3 class="accHead"><a href="#">My Bookings</a></h3>
		<div>
		<c:import url="/members/portal.myBookings.msp"/>
		</div>
	</div>
	
	<div>
		<h3 class="accHead"><a href="#">My Results</a></h3>
		<div>
		<c:import url="/members/portal.myResults.msp"/>
		</div>
	</div>
	
	<!--div>
		<div class="accLink ui-accordion-header ui-state-default ui-corner-all">
		<a href="#" target="_blank"><span class="ui-icon ui-icon-extlink"></span>My Handicap</a>
		</div>
	</div-->
	
	<div>
		<h3 class="accHead"><a href="#">Leaderboards</a></h3>
		<div>
		<c:import url="/members/portal.competition.msp"/>
		</div>
	</div>
	
	<div>
		<h3 class="accHead"><a href="#">Playing Partners</a></h3>
		<div>
		<c:import url="/members/portal.myPartners.msp"/>
		</div>
	</div>
	
	<div>
		<div class="accLink ui-accordion-header ui-state-default ui-corner-all">
		<a href="http://www.bbc.co.uk/weather/sp5" target="_blank"><span class="ui-icon ui-icon-extlink"></span>Weather</a>
		</div>
	</div>
	
</div>
<!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->