<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Miclub Plugin Demo: Members Section</title>
<miclub:property-is-true key="wordpress.enabled">
<c:set scope="request" var="login_url"><miclub:property key="wordpress.login.url"/>?name=<c:out value='${user_membership_number}' /></c:set>
<script type="text/javascript" src="<c:out value='${login_url}' />"></script>
</miclub:property-is-true>
<!-- InstanceEndEditable -->
<%@ include file="/msp/headM.jsp"%>
<!-- InstanceBeginEditable name="Body" --> 
<style type="text/css">
#menu ul > #menu-item-401 > a span{border-bottom:1px dotted #ccc;padding-bottom:0;}
</style>
<c:import url="/jsp/portal/administrator.jsp"/>

<div id="pageContent" class="clear">
<h1 id="welcome">Welcome <span class="name"><c:out value="${greetingName}" /></span></h1>
<span id="date">
<script type="text/javascript">
<!--
today = new Date
weekDayName = new Array ("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
monthName = new Array ("January","February","March","April","May","June","July","August","September","October","November","December")
function printDate()
{
document.write(weekDayName[today.getDay()]+ ", " + monthName[today.getMonth()] + " " + today.getDate())
}
-->
</script>
<script type="text/javascript">printDate()</script>
</span>
<div id="left-column">
<div id="portal-col">
   
   
    <div class="portal-box portal-details">
        <h2 class="portal-head"><miclub:message key="TITLE_PORTAL_MY_DETAILS" /></h2>
        <c:import url="/members/portal.myDetails.msp"/>
    </div>
    
       <div class="portal-box portal-bookings">
    <h2 class="portal-head"><miclub:message key="TITLE_PORTAL_MY_BOOKINGS" /></h2>
    <c:import url="/members/portal.myBookings.msp"/>
    <!--span class="portalLink"><a href="/members/bookings/index.msp">View Fixtures</a></span-->
    </div>
    
    <div class="portal-box portal-results">
        <h2 class="portal-head"><miclub:message key="TITLE_PORTAL_MY_RESULTS" /></h2>
        <c:import url="/members/portal.myResults.msp"/>
    </div> 
    
       <div class="portal-box portal-partners">
        <h2 class="portal-head"><miclub:message key="TITLE_PORTAL_MY_PARTNERS" /></h2>
        <c:import url="/members/portal.myPartners.msp"/>
    </div>
  
</div>

<div id="portal-col">
     
        <div class="portal-box portal-prize">
        <h2 class="portal-head">Prize Account</h2>
        <c:import url="/members/bookings/prize.account.msp"/>
    </div> 
    
     <div class="portal-box portal-account">
        <h2 class="portal-head"><miclub:message key="TITLE_PORTAL_MY_ACCOUNT" /></h2>
        <c:import url="/members/portal.myaccount.msp"/>
    </div> 
    
     <div class="portal-box portal-account-3rd">
        <h2 class="portal-head">My Account</h2>
        <span class="portalLink"><a href="/members/accounts.mhtml" target="_blank">View Your Account &raquo;</a></span>
    </div>   
   
   <div class="portal-box portal-details-3rd">
        <h2 class="portal-head">My Details</h2>
        <span class="portalLink"><a href="/members/details.mhtml" target="_blank">View Your Details &raquo;</a></span>
    </div>   
    
     <miclub:property-is-true key="membership.messaging.enabled">
    <div class="portal-box portal-messages">
        <h2 class="portal-head">My Messages</h2>
        <c:import url='/members/portal.messages.msp'/>
    </div>     
    </miclub:property-is-true>
    
     

 
</div>
</div><div id="right-column">
    <div class="portal-box">
      <a href="/cms/members-home/" class="portal-links">Members Home</a>
      <a href="/members/bookings/index.msp" class="portal-links">Bookings</a>
          <a href="/members/details/handicap.action.xsp" target="_blank" class="portal-links">My Handicap &raquo;</a>
          <a href="/members/directory/index.msp" class="portal-links">Directory</a> 
          <a href="/security/logout.msp" class="portal-links">Logout</a> 
    </div>
   
    <miclub:property-is-true key="competition.golf.enabled">
    <miclub:property-is-true key="competition.golf.leaderboard.enabled">  
    <div class="portal-box portal-leaderboards">
        <h2 class="portal-head">Leaderboards</h2>    
        <c:import url="/members/portal.competition.msp"/>
    </div>
    </miclub:property-is-true>
    </miclub:property-is-true>
</div></div>


</div>

<!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->