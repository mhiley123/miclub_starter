<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Leaderboard for <c:out value="${competition.name}" /></title>
<link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css">
<link href="/style/custom.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="750" align="center" cellpadding="0" cellspacing="0">
<tr>
<td><miclub:import url="/jsp/golf/competition/leaderboard.jsp" /></td>
</tr>
</table>
</body>
</html>
