<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<html><head>
<!-- InstanceBegin template="/Templates/PrivateClub-PopUp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Leaderboard</title>
<!-- InstanceEndEditable -->
<!-- miClub Javascript Library... do not edit or remove -->
<script type="text/javascript" src="/scripts/miclub.js"></script>
<script type="text/javascript" src="/scripts/prototype.js"></script>
<script type="text/javascript" src="/scripts/scriptaculous.js"></script>
<script type="text/javascript" src="/scripts/liveLeaderboard.js"></script>
<!-- miClub product stylesheet... do not edit or remove -->
<!--link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css" /-->
<link href="/style/productStyle/liveLeaderboard.css" rel="stylesheet" type="text/css" />
<!-- customise your own style sheet for this site -->
<!--link href="/style/custom.css" rel="stylesheet" type="text/css" /-->
<style type="text/css">
<!--
body {
	margin-top: 4px;
	background-color:#FFFFFF !important;
}
-->
</style>
</head>
<body>
<!-- InstanceBeginEditable name="Body" -->
<c:import url="/jsp/golf/competition/liveLeaderboard.jsp" />
<!-- InstanceEndEditable -->			
<div id="marqueeLB" style="display:none;color:red;"><marquee scrolldelay="100"><miclub:message key="MARQUEE_3"/></marquee></div>
<script type="text/javascript">
$('marquee').appendChild($('marqueeLB').show());
</script></body>
<!-- InstanceEnd -->
</html>
<%out.flush();%>
