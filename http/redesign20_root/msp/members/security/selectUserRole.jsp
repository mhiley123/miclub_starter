<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib uri="/miclub-security" prefix="miclub-security"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head><!-- InstanceBegin template="/Templates/PrivateClub-Members.dwt" codeOutsideHTMLIsLocked="false" -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club : Select User or Security Role.</title>
<!-- InstanceEndEditable -->
<%@ include file="/msp/headM.jsp"%>
<!-- InstanceBeginEditable name="Body" --><c:import url="/jsp/members/security/selectUserRole.jsp"/><!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->