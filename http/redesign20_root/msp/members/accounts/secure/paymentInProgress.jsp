<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html><head><!-- InstanceBegin template="/Templates/PrivateClub-NoNav.dwt" codeOutsideHTMLIsLocked="false" -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title><miclub:message key="FIELD_CLUB_NAME"/> : <miclub:message key="TEXT_MAKE_PAYMENT"/></title>
<!-- InstanceEndEditable -->
<c:import url="/msp/headNN.jsp"/>
<!-- InstanceBeginEditable name="Body" --><c:import url="/jsp/accounts/secure/paymentInProgress.jsp"/><!-- InstanceEndEditable -->
<%@ include file="/msp/footQ.jsp"%><!-- InstanceEnd -->