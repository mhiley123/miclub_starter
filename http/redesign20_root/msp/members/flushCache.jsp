<%@ taglib prefix="cache" uri="http://www.opensymphony.com/oscache" %>

<%-- HTML Headers, before the title --%>
<cache:flush key="htmlMemberHeader"  scope="application" />
<cache:flush key="htmlGuestHeader"   scope="application" />
<cache:flush key="htmlKioskHeader"   scope="application" />
<cache:flush key="htmlNoNavHeader"   scope="application" />
<cache:flush key="htmlPopupHeader"   scope="application" />

<%-- Club headers, after the title --%>
<cache:flush key="clubMemberHeader" scope="application" />
<cache:flush key="clubGuestHeader"  scope="application" />
<cache:flush key="clubKioskHeader"  scope="application" />
<cache:flush key="clubNoNavHeader"  scope="application" />
<cache:flush key="clubPopupHeader"  scope="application" />

<%-- Club footers, at the end --%>
<cache:flush key="clubMemberFooter"  scope="application" />
<cache:flush key="clubGuestFooter"   scope="application" />
<cache:flush key="clubKioskFooter"   scope="application" />
<cache:flush key="clubNoNavFooter"   scope="application" />
<cache:flush key="clubPopupFooter"   scope="application" />

Cache cleared.