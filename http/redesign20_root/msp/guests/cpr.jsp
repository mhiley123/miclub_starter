<%@ taglib prefix="miclub-msp" uri="/miclub-msp"%>
<%@ taglib prefix="cache"uri="http://www.opensymphony.com/oscache" %>

<miclub-msp:htmlHeader />

<cache:cache key="clubHeader" scope="application">
	<miclub-msp:clubHeader />
</cache:cache>

<h2>Pool cleaned.</h2>

<cache:cache key="clubFooter" scope="application">
	<miclub-msp:clubFooter />
</cache:cache>

<%out.flush();%>
