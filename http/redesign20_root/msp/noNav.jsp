<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %> 
<%@ page import="au.com.miclub.framework.servlet.actions.MspView"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><%=request.getAttribute(MspView.ATTR_TITLE) %></title>
<script type="text/javascript" src="/scripts/miclub.js"></script>
<link rel="stylesheet" type="text/css" href="/style/productStyle/privateClubProduct.css" />
<link rel="stylesheet" type="text/css" media="all" href="http://plugin.miclub.com.au/cms/wp-content/themes/plugin-demo/style.css" />
<!-- start /html -->
</head>
<!-- end /html -->
<body class="page page-id-25 page-template page-template-prodGen page-template-prodGen-php">
	<div id="wrapper">
		<div id="header" class="clearfix">
			<div id="header-top">
				<a href="http://plugin.miclub.com.au/cms" class="logo">
					<img src="http://plugin.miclub.com.au/cms/wp-content/themes/plugin-demo/images/logo.png" alt="Plugin" />
				</a>
			</div>
						</div>
			<div id="main" class="clearfix"><miclub:import url='<%=(String)request.getAttribute(MspView.ATTR_PAGE) %>'/>
		</div>
		<div id="footer" class="clearfix">
			<p class="copyright">This is a standard plugin footer - any changes will need to be quoted separately</p>
		</div>
	</div>
	<!-- 30 queries -->
				<script>
			jQuery('#menu-toggle strong').click(function() {
				jQuery('#menu-toggle strong').toggleClass("toggled-on");
				jQuery('#nav').toggleClass("toggled-on");
			});
			</script>
</body>
</html>