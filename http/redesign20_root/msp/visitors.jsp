<!-- Server side code required for product pages to function correctly... do not edit or remove -->
<%@ taglib prefix="miclub" uri="/miclub" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ page import="au.com.miclub.framework.servlet.actions.MspView"%>

<!-- End of Server side code -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html><head><!-- InstanceBegin template="/Templates/PrivateClub-Guests.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title><%=request.getAttribute(MspView.ATTR_TITLE) %></title>

<!-- miClub product stylesheet... do not edit or remove -->
<link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css"> 
<!-- customise your own style sheet for this site -->
<link href="/style/custom.css" rel="stylesheet" type="text/css"> 
</head>
<body>
<div class="page">
  <table width="750" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table border="0" align="center" cellpadding="0" cellspacing="0" class="bodyprefix">
          <tr class="bodyprefix" height="50">
            <td class="bodyprefix"><img src="/images/exampleHeader.jpg" width="750" height="100"/></td>
          </tr>
          <tr>
            <td height="24" valign="top" background="/images/exampleBackground.gif" class="bodyprefix"> <table width="750" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><div align="center">| <a href="/welcome/index.mhtml">Home</a> | <a href="/members/index.msp">Members</a> | <a href="/security/create.member.action.xsp">Register As A Public Member</a> | <a href="/guests/genericitem/index.msp">Business Listings</a></div></td>
              </tr>
              <tr>
                <td><!-- InstanceBeginEditable name="Body" --><miclub:import url="<%=(String)request.getAttribute(MspView.ATTR_PAGE) %>" /><!-- InstanceEndEditable --></td>
              </tr>
            </table></td>
          </tr>
          <tr>
                        <td><!-- InstanceBeginEditable name="Body" -->

<miclub:import url="<%=(String)request.getAttribute(MspView.ATTR_PAGE) %>" />

							<!-- InstanceEndEditable --></td>
          </tr>
      </table></td>
    </tr>
  </table>
</div>
</body>
<!-- InstanceEnd --></html>
<!-- Server side code required for product pages to function correctly... do not edit or remove -->

<%out.flush();%>
<!-- End of Server side code -->