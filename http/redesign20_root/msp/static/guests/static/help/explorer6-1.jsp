<!-- Server side code required for product pages to function correctly... do not edit or remove -->
<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!-- End of Server side code -->
<html><!-- InstanceBegin template="/Templates/PrivateClub-PopUp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club</title>
<style type="text/css">
p { font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-style: normal;}
li { font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-style: normal;}
a.loginhelp { font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-style: normal;
}
.loginhelp_title1 {
font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
font-style: normal;
font-weight: bold;
color: #666666;
background-color: #cccccc;
height: 35px;
}
.loginhelp_title2 {
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-style: normal;
font-weight: bold;
color: #666666;
background-color: #e0e0e0;
height: 35px;
}
.table1 {
border-top-width: thin;
border-right-width: thin;
border-bottom-width: thin;
border-left-width: thin;
border-top-style: solid;
border-right-style: solid;
border-bottom-style: solid;
border-left-style: solid;
border-top-color: #848484;
border-right-color: #848484;
border-bottom-color: #848484;
border-left-color: #848484;
padding-top: 3px;
padding-right: 3px;
padding-bottom: 3px;
padding-left: 3px;
}
</style>
<!-- InstanceEndEditable -->
<!-- miClub Javascript Library... do not edit or remove -->
<script type="text/javascript" src="/scripts/miclub.js"></script>
<!-- miClub product stylesheet... do not edit or remove -->
<link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css">
<!-- customise your own style sheet for this site -->
<link href="/style/custom.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
margin-top: 4px;
}
-->
</style>
</head>
<body>
<!-- InstanceBeginEditable name="Body" -->
<table width="95%" border="0" align="center" cellpadding="2" cellspacing="0" class="table1">
<tr>
<td><div align="left"><a href="index.msp" class="loginhelp"><img width="17" height="13" src="/icons/help/previous.gif" border="0"/>&nbsp;Main </a></div></td>
<td><div align="center"><a href="explorer55-1.msp" class="loginhelp">Internet Explorer 5.5</a></div></td>
<td><div align="center"><a href="netscape1.msp" class="loginhelp">Netscape 7.0</a></div></td>
<td><div align="right"><a href="explorer6-2.msp" class="loginhelp">Step 2 &nbsp; <img width="17" height="13" src="/icons/help/next.gif" border="0"/></a></div></td>
</tr>
<tr bgcolor="#cccccc">
<td colspan="4" class="loginhelp_title1" align="center">Browser Settings - Internet Explorer 6</td>
</tr>
<tr>
<td colspan="4" valign="top"><p> To check your settings, we have provided examples of the windows you will need to access.</p>
<p> Please follow the steps below:</p>
<blockquote>
<blockquote>
<p align="left"><a href="#step1">Step 1: Internet Options </a></p>
<p align="left"><a href="explorer6-2.msp" class="loginhelp">Step 2: Security Settings </a></p>
<p align="left"><a href="explorer6-3.msp" class="loginhelp">Step 3: Clear History and Temporary Internet Files </a></p>
<p align="left"><a href="explorer6-4.msp" class="loginhelp">Step 4: Privacy Settings </a></p>
<p align="left"><a href="explorer6-5.msp" class="loginhelp">Step 5: Close and reopen your browser </a></p>
</blockquote>
</blockquote>
<p>&nbsp; </p></td>
</tr>
<tr bgcolor="#cccccc">
<td colspan="4" align="center" class="loginhelp_title2"><strong> <a name="step1"></a>Step 1 : Internet Options</strong></td>
</tr>
<tr>
<td colspan="4"><ul>
<li>Select 'Tools' from the browser menu bar.</li>
<li>Select 'Internet Options' from the drop down list.</li>
</ul>
<br />
<table align="center" cellpadding="0">
<tr>
<td><p align="left"><img width="136" height="113" src="/icons/help/clip_image005.gif" alt="Internet Explorer 6 Picture 1"></p></td>
</tr>
</table>
<p>&nbsp;</p></td>
</tr>
<tr>
<td><div align="left"><a href="index.msp" class="loginhelp"><img width="17" height="13" src="/icons/help/previous.gif" border="0"/>&nbsp;Main </a></div></td>
<td><div align="center"><a href="explorer55-1.msp" class="loginhelp">Internet Explorer 5.5</a></div></td>
<td><div align="center"><a href="netscape1.msp" class="loginhelp">Netscape 7.0</a></div></td>
<td><div align="right"><a href="explorer6-2.msp" class="loginhelp">Step 2 &nbsp; <img width="17" height="13" src="/icons/help/next.gif" border="0"/></a></div></td>
</tr>
</table>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<!-- Server side code required for product pages to function correctly... do not edit or remove -->
<%out.flush();%>
<!-- End of Server side code -->
