<!-- Server side code required for product pages to function correctly... do not edit or remove -->
<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!-- End of Server side code -->
<html><!-- InstanceBegin template="/Templates/PrivateClub-PopUp.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="pageTitle" -->
<title>Private Club</title>
<style type="text/css">
p { font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-style: normal;}
li { font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-style: normal;}
a.loginhelp { font-family: Arial, Helvetica, sans-serif;
font-size: 12px;
font-style: normal;
}
.loginhelp_title1 {
font-family: Arial, Helvetica, sans-serif;
font-size: 18px;
font-style: normal;
font-weight: bold;
color: #666666;
background-color: #cccccc;
height: 35px;
}
.loginhelp_title2 {
font-family: Arial, Helvetica, sans-serif;
font-size: 14px;
font-style: normal;
font-weight: bold;
color: #666666;
background-color: #e0e0e0;
height: 35px;
}
.table1 {
border-top-width: thin;
border-right-width: thin;
border-bottom-width: thin;
border-left-width: thin;
border-top-style: solid;
border-right-style: solid;
border-bottom-style: solid;
border-left-style: solid;
border-top-color: #848484;
border-right-color: #848484;
border-bottom-color: #848484;
border-left-color: #848484;
padding-top: 3px;
padding-right: 3px;
padding-bottom: 3px;
padding-left: 3px;
}
</style>
<!-- InstanceEndEditable -->
<!-- miClub Javascript Library... do not edit or remove -->
<script type="text/javascript" src="/scripts/miclub.js"></script>
<!-- miClub product stylesheet... do not edit or remove -->
<link href="/style/productStyle/privateClubProduct.css" rel="stylesheet" type="text/css">
<!-- customise your own style sheet for this site -->
<link href="/style/custom.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
margin-top: 4px;
}
-->
</style>
</head>
<body>
<!-- InstanceBeginEditable name="Body" --><br>
<table width="95%" border="0" align="center" cellpadding="3" cellspacing="0" bordercolor="#cccccc" class="table1">
<tr>
<td colspan="2" bgcolor="#cccccc" class="loginhelp_title1" align="center">Login Help</td>
</tr>
<tr>
<td colspan="2"><p>If you are having difficulties logging on we firstly recommend you log in without using the browser saved password. For example, replace the dots ( <img src="/icons/help/password.gif" width="66" height="20" align="absmiddle" />) in the password text box by deleting them and typing your password.</p>
<p> If you have no success in logging on, we then recommend you take a few minutes to check your browser settings using our online guide. Our recommended browsers are listed below.</p>
<p> To view our suggested settings, select your browser from the following list.</p></td>
</tr>
<tr>
<td class="loginhelp_title2" align="center">Recommended Browsers and Settings</td>
</tr>
<tr>
<td colspan="2"><table width="100%" border="0" align="center" cellpadding="5" cellspacing="0">
<tr>
<td><p align="center"><a href="explorer6-1.msp"> Internet Explorer 6.0</a></p>
<p align="center"><a href="explorer55-1.msp">Internet Explorer 5.5</a></p>
<p align="center"><a href="netscape1.msp">Netscape 7.0</a> </td>
</tr>
</table></td>
</tr>
<tr>
<td colspan="2" class="loginhelp_title2" align="center"><strong> Mac Users </strong></td>
</tr>
<tr>
<td colspan="2"><p> We recommend you use Safari. Follow this link to <a href="http://www.apple.com/safari/download/" target="_blank">download the latest version available from Apple</a>. </p></td>
</tr>
<tr>
<td colspan="2" class="loginhelp_title2" align="center"><strong> Update your browser software</strong></td>
</tr>
<tr>
<td colspan="2"><p>If you are not currently running one of the recommended browser versions, you may wish to upgrade by clicking the appropriate link below.</p></td>
</tr>
<tr>
<td colspan="2"><table border="0" align="center" cellpadding="3" cellspacing="10">
<tr>
<td><p align="center"><a href="http://browser.netscape.com/ns8/" target="_blank" ><img src="/icons/help/clip_image001.gif" width="90" height="30" border="0"></a><a href="http://browser.netscape.com/ns8/" target="_blank"><br />
Get Netscape Navigator</a> </td>
<td><p align="center"><a href="http://www.microsoft.com/windows/ie/default.mspx" target="_blank"><img src="/icons/help/clip_image002.gif" width="88" height="31" border="0"></a><a href="http://www.microsoft.com/windows/ie/default.mspx" target="_blank"><br />
Get Internet Explorer</a> </td>
</tr>
</table></td>
</tr>
</table>
<p>&nbsp;</p>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
<!-- Server side code required for product pages to function correctly... do not edit or remove -->
<%out.flush();%>
<!-- End of Server side code -->
