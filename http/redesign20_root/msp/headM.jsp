<script type="text/javascript" src="/scripts/miclub.js"></script>
<link rel="stylesheet" type="text/css" href="/style/productStyle/privateClubProduct.css" />
<link rel="stylesheet" type="text/css" media="all" href="http://plugin.miclub.com.au/cms/wp-content/themes/plugin-demo/style.css" />
<!-- start /html -->
</head>
<!-- end /html -->
<body class="page page-id-25 page-template page-template-prodGen page-template-prodGen-php logged-in">
	<div id="wrapper">
		<div id="header" class="clearfix">
			<div id="header-top">
				<a href="http://plugin.miclub.com.au/cms" class="logo">
					<img src="http://plugin.miclub.com.au/cms/wp-content/themes/plugin-demo/images/logo.png" alt="Plugin" />
				</a>
			</div>
			<ul id="nav" class="membersMenu"><li id="menu-item-59" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children arIco"><a href="#"><div class="left"></div><span>Standard Members Menu</span><div class="right"></div></a>
<ul class="sub-menu">
	<li id="menu-item-76" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/extra-features-available/"><div class="left"></div><span>Extra Features Available</span><div class="right"></div></a></li>
</ul>
</li>
<li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/home/"><div class="left"></div><span>Home</span><div class="right"></div></a></li>
<li id="menu-item-60" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children arIco"><a href="#"><div class="left"></div><span>Members Home Pages Options</span><div class="right"></div></a>
<ul class="sub-menu">
	<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/members-home/"><div class="left"></div><span>Members Home Masonry</span><div class="right"></div></a></li>
	<li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/members-news-list/"><div class="left"></div><span>Members News List Layout</span><div class="right"></div></a></li>
</ul>
</li>
<li id="menu-item-70" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/members/portal.msp"><div class="left"></div><span>My Information</span><div class="right"></div></a></li>
<li id="menu-item-68" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/members/bookings/index.xsp?booking_resource_id=3000000"><div class="left"></div><span>Bookings</span><div class="right"></div></a></li>
<li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/members/directory/index.xsp"><div class="left"></div><span>Directory</span><div class="right"></div></a></li>
<li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page"><a target="_blank" href="/members/details/handicap.action.xsp"><div class="left"></div><span>My Handicap</span><div class="right"></div></a></li>
<li id="menu-item-58" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/security/logout.msp"><div class="left"></div><span>Logout</span><div class="right"></div></a></li>
</ul>			</div>
			<div id="main" class="clearfix">