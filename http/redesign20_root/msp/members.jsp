<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="/miclub" prefix="miclub"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %> 
<%@ page import="au.com.miclub.framework.servlet.actions.MspView"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><%=request.getAttribute(MspView.ATTR_TITLE) %></title>
<script type="text/javascript" src="/scripts/miclub.js"></script>
<link rel="stylesheet" type="text/css" href="/style/productStyle/privateClubProduct.css" />
<link rel="stylesheet" type="text/css" media="all" href="http://plugin.miclub.com.au/cms/wp-content/themes/plugin-demo/style.css" />
<!-- start /html -->
</head>
<!-- end /html -->
<body class="page page-id-25 page-template page-template-prodGen page-template-prodGen-php logged-in">
	<div id="wrapper">
		<div id="header" class="clearfix">
			<div id="header-top">
				<a href="http://plugin.miclub.com.au/cms" class="logo">
					<img src="http://plugin.miclub.com.au/cms/wp-content/themes/plugin-demo/images/logo.png" alt="Plugin" />
				</a>
			</div>
			<ul id="nav" class="membersMenu"><li id="menu-item-59" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children arIco"><a href="#"><div class="left"></div><span>Standard Members Menu</span><div class="right"></div></a>
<ul class="sub-menu">
	<li id="menu-item-76" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/extra-features-available/"><div class="left"></div><span>Extra Features Available</span><div class="right"></div></a></li>
</ul>
</li>
<li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/home/"><div class="left"></div><span>Home</span><div class="right"></div></a></li>
<li id="menu-item-60" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children arIco"><a href="#"><div class="left"></div><span>Members Home Pages Options</span><div class="right"></div></a>
<ul class="sub-menu">
	<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/members-home/"><div class="left"></div><span>Members Home Masonry</span><div class="right"></div></a></li>
	<li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://plugin.miclub.com.au/cms/members-news-list/"><div class="left"></div><span>Members News List Layout</span><div class="right"></div></a></li>
</ul>
</li>
<li id="menu-item-70" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/members/portal.msp"><div class="left"></div><span>My Information</span><div class="right"></div></a></li>
<li id="menu-item-68" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/members/bookings/index.xsp?booking_resource_id=3000000"><div class="left"></div><span>Bookings</span><div class="right"></div></a></li>
<li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/members/directory/index.xsp"><div class="left"></div><span>Directory</span><div class="right"></div></a></li>
<li id="menu-item-67" class="menu-item menu-item-type-post_type menu-item-object-page"><a target="_blank" href="/members/details/handicap.action.xsp"><div class="left"></div><span>My Handicap</span><div class="right"></div></a></li>
<li id="menu-item-58" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="/security/logout.msp"><div class="left"></div><span>Logout</span><div class="right"></div></a></li>
</ul>			</div>
			<div id="main" class="clearfix"><miclub:import url='<%=(String)request.getAttribute(MspView.ATTR_PAGE) %>'/>
		</div>
		<div id="footer" class="clearfix">
			<p class="copyright">This is a standard plugin footer - any changes will need to be quoted separately</p>
		</div>
	</div>
	<!-- 28 queries -->
	<script type='text/javascript' src='http://plugin.miclub.com.au/cms/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='http://plugin.miclub.com.au/cms/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script>var pltNewTabURLs = ["\/members\/details\/handicap.action.xsp"];(function(){(function(e){var t;t=e.jQueryWP||e.jQuery;return t(function(e){return typeof e.fn.on=="function"?e("body").on("click","a",function(t){var n;n=e(this);if(e.inArray(n.attr("href"),pltNewTabURLs)>-1)return n.attr("target","_blank")}):typeof console!="undefined"&&console!==null?console.log("Page Links To: Some other code has overridden the WordPress copy of jQuery. This is bad. Because of this, Page Links To cannot open links in a new window."):void 0})})(window)}).call(this);</script>			<script>
			jQuery('#menu-toggle strong').click(function() {
				jQuery('#menu-toggle strong').toggleClass("toggled-on");
				jQuery('#nav').toggleClass("toggled-on");
			});
			</script>
</body>
</html>