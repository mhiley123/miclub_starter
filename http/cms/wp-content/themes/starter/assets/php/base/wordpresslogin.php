<?php
//
// Custom Log in screen
// Location of image can be changed in the style below
//

function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
              /*-- Change image url - Image size should be 84px wide--*/
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_wp.png);
        }
	</style>
<?php }

add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
		$blogTitle = bloginfo('name');
      	//Change link title
    	return $blogTitle;
}
add_filter( 'login_headertitle', '' );


	// CSS classnames
function the_category_unlinked($separator = ' ') {
    $categories = (array) get_the_category();
    
    $thelist = '';
    foreach($categories as $category) {    // concate
        $thelist .= $separator . $category->category_nicename;
    }
    echo $thelist;
}
?>