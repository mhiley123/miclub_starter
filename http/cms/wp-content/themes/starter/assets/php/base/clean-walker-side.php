<?php

// members side menu
class Clean_Walker_Nav_Sub_Menu_Members extends Walker_Nav_Menu {

    var $found_parents = array();

    function start_el(&$output, $item, $depth, $args) {

        global $wp_query;

        $parent_item_id = 0;

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        if ( ($item->menu_item_parent==0) && (strpos($class_names, 'current-menu-parent')) ) {
            $output.= '<li>';
        }

        if (strpos($class_names, 'current-menu-item')
            || strpos($class_names, 'current-menu-parent')
            || strpos($class_names, 'current-menu-ancestor')
            || (is_array($this->found_parents) && in_array( $item->menu_item_parent, $this->found_parents )) ) {

            $this->found_parents[] = $item->ID;

            if($item->menu_item_parent!=$parent_item_id){

                if ( !get_post_meta( $item->object_id , '_guests_only' , true ) || !is_user_logged_in() ) {
                    $output .= $indent . '<li ' . $value . $class_names .'>';
                }

                $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
                $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
                $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
                $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

                $item_output = $args->before;
                $item_output .= '<a'. $attributes .'>';
                $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
                $item_output .= '</a>';
                $item_output .= $args->after;

                if ( !get_post_meta( $item->object_id, '_guests_only' , true ) || !is_user_logged_in() ) {
                    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
                }
            }

        }
    }

    function end_el(&$output, $item, $depth) {
        if ( is_array($this->found_parents) && in_array( $item->ID, $this->found_parents ) && !get_post_meta( $item->object_id, '_guests_only' , true ) || !is_user_logged_in() ) {
            $output .= "</li>\n";
        }
    }

    function end_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        if (substr($output, -22)=="<ul class=\"sub-menu\">\n") {
            $output = substr($output, 0, strlen($output)-23);
        } else {
            $output .= "$indent</ul>\n";
        }
    }
}

// guest side menu
class Clean_Walker_Nav_Sub_Menu_Guests extends Walker_Nav_Menu {

    var $found_parents = array();

    function start_el(&$output, $item, $depth, $args) {

        global $wp_query;

        $parent_item_id = 0;

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        if ( ($item->menu_item_parent==0) && (strpos($class_names, 'current-menu-parent')) ) {
            $output.= '<li>';
        }

        if (strpos($class_names, 'current-menu-item')
            || strpos($class_names, 'current-menu-parent')
            || strpos($class_names, 'current-menu-ancestor')
            || (is_array($this->found_parents) && in_array( $item->menu_item_parent, $this->found_parents )) ) {

            $this->found_parents[] = $item->ID;

            if($item->menu_item_parent!=$parent_item_id){

                if ( !get_post_meta( $item->object_id , '_members_only' , true ) || is_user_logged_in() ) {
                    $output .= $indent . '<li ' . $value . $class_names .'>';
                }

                $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
                $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
                $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
                $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

                $item_output = $args->before;
                $item_output .= '<a'. $attributes .'>';
                $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
                $item_output .= '</a>';
                $item_output .= $args->after;

                if ( !get_post_meta( $item->object_id, '_members_only' , true ) || is_user_logged_in() ) {
                    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
                }
            }
        }
    }

    function end_el(&$output, $item, $depth) {
        if ( is_array($this->found_parents) && in_array( $item->ID, $this->found_parents ) && !get_post_meta( $item->object_id, '_members_only' , true ) || is_user_logged_in() ) {
            $output .= "</li>\n";
        }
    }

    function end_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        if (substr($output, -22)=="<ul class=\"sub-menu\">\n") {
            $output = substr($output, 0, strlen($output)-23);
        } else {
            $output .= "$indent</ul>\n";
        }
    }
}

?>