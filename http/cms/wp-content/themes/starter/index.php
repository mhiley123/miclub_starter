<?php get_header(); ?>

<div id="left-column">
	<div class="content-wp <?php category-name; ?>" id="archive">
		<h1>News Archive</h1>
		<?php get_template_part('loop'); ?>
		<?php get_template_part('pagination'); ?>
	</div>
</div>
<?php include ('sidebar-news.php'); ?>



<?php get_footer(); ?>