<?php get_header(); ?>

<div id="left-column">
	<div class="content-wp">
		<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
		<?php /* If this is a category archive */ if (is_category()) { ?>
		<h1 class="archive_title_name"><?php printf(__('%s Articles'), single_cat_title('', false)); ?></h1>
		<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
		<h1 class="archive_title_name"><?php printf(__('Tag "%s"'), single_tag_title('', false) ); ?></h1>
		<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h1 class="archive_title_name"><?php printf(_c('%s Daily archive'), get_the_time(__('F jS, Y'))); ?></h1>
		<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h1 class="archive_title_name"><?php printf(_c('%s Monthly archive'), get_the_time(__('F, Y'))); ?></h1>
		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h1 class="archive_title_name"><?php printf(_c('%s Yearly archive'), get_the_time(__('Y'))); ?></h1>
		<?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h1 class="archive_title_name"><?php _e('Author Archive'); ?></h1>
		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h1 class="archive_title_name"><?php _e('Blog Archives'); ?></h1>
		<?php } ?>
		<?php get_template_part('loop'); ?>
		<?php get_template_part('pagination'); ?>
	</div>
</div>
<?php include ('sidebar-news.php'); ?>



<?php get_footer(); ?>