<!DOCTYPE html>
<html><head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<title>
<?php
global $page, $paged; 
/*echo the_field('hole_name');*/
wp_title('|', true, 'right');
bloginfo('name');
$site_description = get_bloginfo('description', 'display');
if ( $site_description && ( is_home() || is_front_page()))
	echo " | $site_description";
if ($paged >= 2 || $page >= 2) 
	echo ' | ' . sprintf( __('Page %s'), max($paged, $page));
?> 
</title>
<meta http-equiv="Content-language" content="<?php bloginfo('language'); ?>" /> 
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<!-- Need to include these JS files in the header (instead of footer) -->
<script type='text/javascript' src='/cms/wp-includes/js/jquery/jquery.js?ver=1.7.2'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script><!-- Modernizr -->

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/assets/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/media.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>"/>
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<?php wp_head(); ?> 
<!-- insert google anlytics here -->
</head>
<body <?php if ( current_user_can('edit_post') ) { body_class('custom-wp logged-in-editor'); } else { body_class('custom-wp'); } ?>>
	<?php if ( current_user_can('edit_post') ) { ?>
	<?php include ('admin-bar.php'); ?>
	<?php } ?>
	<div class="wrapper">
		<div class="header clearfix">
        	<div class="container">
                <div class="logo">
                    <a href="<?php bloginfo('url'); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="<?php strip_tags(bloginfo('name')); ?>" />
                    </a>
                </div>
                
                <div class="main-navigation">
                    <div id="menu-toggle">
                        <strong>Menu</strong> 
                    </div>
    
                    <div class="big-menu">
                        <?php if ( is_user_logged_in() ) { ?>
                            <?php wp_nav_menu(array(
                                'theme_location' => 'secondary',
                                'container' => 'false',
                                'menu_id' => 'header-nav',
                                'menu_class' => 'members-menu navbar-collapse collapse',
                                'walker' => new Walker_Nav_Menu_CMS_members()
                            )); ?>
                        <?php } else { ?>
                            <?php wp_nav_menu(array('theme_location' => 'primary',
                                'container' => 'false',
                                'menu_id' => 'header-nav',
                                'menu_class' => 'guests-menu navbar-collapse collapse',
                                'walker' => new Walker_Nav_Menu_CMS_guests()
                            )); ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
		</div>
          <?php if ( is_single() || is_category() || is_archive() ) {  ?>
  
			<?php  } else if (has_post_thumbnail()) { ?>
                <?php the_post_thumbnail();?>
            <?php } else { ?>
                   <div class="page-hero"><img src="<?php bloginfo('template_directory'); ?>/images/header.jpg" class="headimg" /></div>
            <?php } ?>
		<div class="main clearfix">
        	<div class="container">
        