<?php
/*
Template Name: Product Template Generator
*/
?>
<?php
if ( is_user_logged_in() ) {
echo '<div style="position:absolute;top:10px;left:50%;width:600px;margin-left:-320px;border:1px solid #f00;background:#faede7;padding:10px 25px;z-index:998;text-align:center;">Members templates have been created <a href="' . wp_logout_url( get_permalink() ) . '">click here</a> to create guests templates and logout.</div>';
}
else {
echo '<div style="position:absolute;top:10px;left:50%;width:600px;margin-left:-320px;border:1px solid #f00;background:#faede7;padding:10px 25px;z-index:999;text-align:center;">Guests templates have been created.</div>';}
?>
<?php

// define site name
$SiteName = "redesign20";   
$cacheDir = "/campbell/" . $SiteName . "/http/" . $SiteName . "_root/msp/";
$jspContent = "<miclub:import url='<%=(String)request.getAttribute(MspView.ATTR_PAGE) %>'/>";
$xhtmlContent = "<ui:insert name='content'/>";
$xhtmlHeadEd = "<ui:insert name='head'/></h:head>";
$jspHeadEd = "</head>";

// if old style menu, make sure the name is correct
$menuDirM = "/campbell/" . $SiteName . "/http/" . $SiteName . "_root/mhtml/both/menuM.jsp";
$menuDirG = "/campbell/" . $SiteName . "/http/" . $SiteName . "_root/mhtml/both/menuG.jsp";

// outputs
$xhtmlFileNameM = $cacheDir."members.xhtml";
$xhtmlFileNameG = $cacheDir."guests.xhtml";
$jspFileNameM = $cacheDir."members.jsp";
$jspFileNameG = $cacheDir."guests.jsp";
$jspFileNameMB = $cacheDir."memberBooking.jsp";
$xhtmlFileNameMB = $cacheDir."memberBooking.xhtml";
$jspFileNameNN = $cacheDir."noNav.jsp";
$xhtmlFileNameNN = $cacheDir."noNav.xhtml";
$headM = $cacheDir."headM.jsp";
$headG = $cacheDir."headG.jsp";
$headNN = $cacheDir."headNN.jsp";
$foot = $cacheDir."footQ.jsp";

if ( is_user_logged_in() ) {
// create members.jsp
ob_start();
$incMenuO = "members";
$incHeadEnd = $jspHeadEd;
include "jspheadQ.php";
include "headQ.php";
echo $jspContent . "\n";
include "footQ.php";
file_put_contents($jspFileNameM,ob_get_contents());
ob_end_flush();

// create members.xhtml
ob_start();
$incMenuO = "members";
$incHeadEnd = $xhtmlHeadEd;
include "xhtmlheadQ.php";
include "headQ.php";
echo $xhtmlContent . "\n";
include "footQ.php";
file_put_contents($xhtmlFileNameM,ob_get_contents());
ob_end_flush();

// create memberBooking.jsp
ob_start();
$incMenuO = "members";
$incHeadEnd = $jspHeadEd;
include "jspheadQ.php";
include "headQ.php";
echo $jspContent . "\n";
include "footQ.php";
file_put_contents($jspFileNameMB,ob_get_contents());
ob_end_flush();

// create memberBooking.xhtml
ob_start();
$incMenuO = "members";
$incHeadEnd = $xhtmlHeadEd;
include "xhtmlheadQ.php";
include "headQ.php";
echo $xhtmlContent . "\n";
include "footQ.php";
file_put_contents($xhtmlFileNameMB,ob_get_contents());
ob_end_flush();

// create headM.jsp
ob_start();
$incMenuO = "members";
$incHeadEnd = $jspHeadEd;
include "headQ.php";
file_put_contents($headM,ob_get_contents());
ob_end_flush();
}

if ( !is_user_logged_in() ) {
// create guests.jsp
ob_start();
$incMenuO = "guests";
$incHeadEnd = $jspHeadEd;
include "jspheadQ.php";
include "headQ.php";
echo $jspContent . "\n";
include "footQ.php";
file_put_contents($jspFileNameG,ob_get_contents());
ob_end_flush();

// create guests.xhtml
ob_start();
$incMenuO = "guests";
$incHeadEnd = $xhtmlHeadEd;
include "xhtmlheadQ.php";
include "headQ.php";
echo $xhtmlContent . "\n";
include "footQ.php";
file_put_contents($xhtmlFileNameG,ob_get_contents());
ob_end_flush();

// create noNav.jsp
ob_start();
$incMenuO = "none";
$incHeadEnd = $jspHeadEd;
include "jspheadQ.php";
include "headQ.php";
echo $jspContent . "\n";
include "footQ.php";
file_put_contents($jspFileNameNN,ob_get_contents());
ob_end_flush();

// create noNav.xhtml
ob_start();
$incMenuO = "none";
$incHeadEnd = $xhtmlHeadEd;
include "xhtmlheadQ.php";
include "headQ.php";
echo $xhtmlContent . "\n";
include "footQ.php";
file_put_contents($xhtmlFileNameNN,ob_get_contents());
ob_end_flush();

// create headG.jsp
ob_start();
$incMenuO = "guests";
$incHeadEnd = $jspHeadEd;
include "headQ.php";
file_put_contents($headG,ob_get_contents());
ob_end_flush();

// create headNN.jsp
ob_start();
$incMenuO = "none";
$incHeadEnd = $jspHeadEd;
include "headQ.php";
file_put_contents($headNN,ob_get_contents());
ob_end_flush();
}

// create foot.jsp
ob_start();
include "footQ.php";
file_put_contents($foot,ob_get_contents());
ob_end_flush();
?>
