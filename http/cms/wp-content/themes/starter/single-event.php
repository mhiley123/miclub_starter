<?php
/**
 * The template for displaying a single event
 *
 ***************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory. 
 *
 * WordPress will automatically prioritise the template in your theme directory.
 ***************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since 1.0.0
 */

//Call the template header
get_header(); ?>

<div id="left-column">
	<div class="content-wp">
		<div class="post-meta">
			<?php while ( have_posts() ) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<h3 class="post-data">
			<!-- Choose a different date format depending on whether we want to include time -->
			<?php if(eo_is_all_day()): ?>
			<!-- Event is all day -->
			<?php $date_format = 'j F Y'; ?>
			<?php else: ?>
			<!-- Event is not all day - include time in format -->
			<?php $date_format = 'j F Y g:ia'; ?>
			<?php endif; ?>
			<?php if(eo_reoccurs()):?>
			<!-- Event reoccurs - is there a next occurrence? -->
			<?php $next =   eo_get_next_occurrence($date_format);?>
			<?php if($next): ?>
			<!-- If the event is occurring again in the future, display the date -->
			<?php printf(__('This event is running from %1$s until %2$s. It is next occurring at %3$s','eventorganiser'), eo_get_schedule_start('d F Y'), eo_get_schedule_last('d F Y'), $next);?>
			<?php else: ?>
			<!-- Otherwise the event has finished (no more occurrences) -->
			<?php printf(__('This event finished on %s','eventorganiser'), eo_get_schedule_last('d F Y',''));?>
			<?php endif; ?>
			<?php else: ?>
			<!-- Event is a single event -->
			<?php printf(__('This event is on %s','eventorganiser'), eo_get_the_start($date_format) );?>
			<?php endif; ?>
			<?php if (current_user_can( 'delete_others_posts' )) {?>  <span>|</span>  <?php edit_post_link('Edit'); ?><?php } ?>
			</h3>
		</div>
		<div class="post-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'eventorganiser' ) . '</span>', 'after' => '</div>' ) ); ?>
		</div>
		<!-- #post-<?php the_ID(); ?> -->			
		<?php endwhile; // end of the loop. ?>
		<p id="back"><a href="javascript: history.go(-1)">&larr; Back to previous page</a></p>
	</div>
</div>
<div id="right-column">
	<h3>Whats On</h3>
	<?php { 
	dynamic_sidebar( 'Events' ); 
	} ?>
	<link rel='stylesheet' id='eventorganiser-jquery-ui-style-css'  href='/cms/wp-content/plugins/event-organiser/css/eventorganiser-admin-fresh.css?ver=1.4' type='text/css' media='all' />
</div>



<?php get_footer(); ?>
