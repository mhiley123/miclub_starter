<div id="right-column">


	<h3>Recent News</h3>
	<ul>
		<?php query_posts(array('posts_per_page' => 7)); if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> <span class="date">(<?php the_time(__('d-m-Y')) ?>)</span></li>
		<?php endwhile; endif; wp_reset_query(); ?>
	</ul>
	<h3>News Archives</h3>
	<ul>
		<?php wp_get_archives('type=monthly&limit=12'); ?>
	</ul>

</div> 