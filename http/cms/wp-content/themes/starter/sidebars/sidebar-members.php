<div class="left-column">

<div class="portal-box">    
    <div class="events-listing">
        <h2>Upcoming events</h2>
  <?php echo do_shortcode('[eo_events numberposts=3 showpastevents="false" group_events_by="series"] <div class="date"><span class="day">%start{d}%</span><span class="month">%start{M}%</span></div><h3><a href="%event_url%">%event_title%</a></h3><p><i class="icon-calendar"></i><strong>Date:</strong> %start{jS M, Y}%<br/><i class="icon-clock"></i><strong>Time:</strong> %start{ g:ia}%<br/><a href="%event_url%" class="read-more">Event Details &raquo;</a></p> [/eo_events]'); ?>
    </div>
</div> 