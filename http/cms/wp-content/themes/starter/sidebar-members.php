<div id="right-column">
  <h1> Std Sidebar Menu</h1>
    <div class="portal-box">
        <div class="portal-links"><a href="/members/portal.msp"  target="_blank" >My Information</a></div>
        <div class="portal-links"><a href="/members/bookings/index.msp"  target="_blank" >Bookings</a></div>
        <div class="portal-links"><a href="/members/details/handicap.action.xsp" target="_blank">My Handicap &raquo;</a></div>
        <div class="portal-links"><a href="/members/directory/index.msp"  target="_blank">Members Directory</a></div> 
        <div class="portal-links"><a href="/security/logout.msp" >logout</a></div>  
    </div>

  <h1> Optional Extra Links</h1>
    <div class="portal-box">
        <div class="portal-links-extra"><a href="#"  target="_blank" >BOM Radar &raquo;</a></div>
        <div class="portal-links-extra"><a href="#">MiScore</a></div>
        <div class="portal-links-extra"><a href="#">Lesson Bookings</a></div>
        <div class="portal-links-extra"><a href="#">Tennis Bookings</a></div>
        <div class="portal-links-extra"><a href="#">Cart Availability*</a></div>
        <div class="portal-links-extra"><a href="#">Course Conditions*</a></div>
        <p class="small"><a href="/cms/extra-features-available/">*Click here to view price information</a></p>
        
    </div>
    
	<hr/>
<div class="portal-box">    
    <div class="events-listing">
        <h2>What's On (Additional Cost)</h2>
  <?php echo do_shortcode('[eo_events numberposts=3 showpastevents="false" group_events_by="series"] <div class="date"><span class="day">%start{d}%</span><span class="month">%start{M}%</span></div><h3><a href="%event_url%">%event_title%</a></h3><p><i class="icon-calendar"></i><strong>Date:</strong> %start{jS M, Y}%<br/><i class="icon-clock"></i><strong>Time:</strong> %start{ g:ia}%<br/><a href="%event_url%" class="read-more">Event Details &raquo;</a></p> [/eo_events]'); ?>
    </div>
</div> 