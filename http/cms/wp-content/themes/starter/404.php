<?php get_header(); ?>

<div id="left-column">
	<h1>404</h1>
	<p class="error404_text">The page <strong><?php echo $_SERVER['REQUEST_URI']; ?></strong> could not be located on this website. We recommend using the navigation bar to get back on track within our site. If you feel you have reached this page in error, please contact a site operator. Thank you!</p>
</div>
<div id="right-column">
	<h3>Lost?</h3>
	<ul class="menu" id="sideNav"><li><ul class="sub-menu"><li><a href="<?php bloginfo('home'); ?>" class="error404_back">Return to the Front Page</a></li></ul></li></ul>
</div>



<?php get_footer(); ?>