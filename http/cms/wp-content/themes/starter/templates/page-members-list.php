<?php 
/* Template Name: Members Home List */
get_header(); ?>



<div class="right-column">
	<h1><?php the_title(); ?></h1>

	<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php endif; ?>
    
    <?php query_posts(array('category_name' => 'members-news' , 'posts_per_page' => 8));
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div <?php post_class('news-item'); ?>>   
		<?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
        	<?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
		<?php } else { ?>
        	<img class="attachment-full wp-post-image" src="<!--?php bloginfo('template_directory'); ?>/images/news-logo.png" alt="<!--?php the_title(); ?>" />
        <?php } ?> 
        
        
        
        <div class="news-text">
            <h2 class="post-title"><a href="<?php the_permalink() ?>" title="<?php printf( esc_attr__('Permalink to %s'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>                
            <div class="post-content" id="post-content_<?php the_ID(); ?>">
                <p><?php the_excerpt(); ?></p>
          </div>
            <small class="post-meta" id="post-meta_<?php the_ID(); ?>">Published on <?php the_time(__('jS F Y')) ?><?php if (current_user_can( 'delete_others_posts' )) {?> <span>|</span> <?php edit_post_link('Edit'); ?><?php } ?></small>
        </div>
    </div>
    <?php endwhile; endif; wp_reset_query(); ?>
    
</div>
<?php include(get_template_directory() . '/sidebars/sidebar-members.php'); ?>
<?php get_footer(); ?>