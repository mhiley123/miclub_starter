<?php 
/* Template Name: Members Home Masonry */
get_header(); ?>
<script src="<?php bloginfo('template_url'); ?>/scripts/masonry.pkgd.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/scripts/imagesloaded.pkgd.js"></script>
<?php if ( is_single() || is_category() || is_archive() ) {  ?>
<?php  } else if (has_post_thumbnail()) { ?>
<?php the_post_thumbnail();?>
<?php } else { ?>
       <div class="page-hero"><img src="<?php bloginfo('template_directory'); ?>/images/header.jpg" class="headimg" /></div>
<?php } ?>  
<script type="text/javascript">    
docReady( function() {
	var container = document.querySelector('#pageContent');

	imagesLoaded( container, function() {
		var msnry = new Masonry( container, {
		// options
		columnWidth: '.grid-sizer',
		gutter: '.gutter-sizer',
		itemSelector: '.news-item',
		});
	
	});

});
</script> 

<div id="left-column">
<h1>Members Home Masonry Layout</h1> 
<div id="pageContent" class="clear">
   <div class="grid-sizer"></div>
    <div class="gutter-sizer"></div>
    <?php query_posts(array('category_name' => 'members-news' , 'posts_per_page' => 8));
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div <?php post_class('news-item'); ?>>   
		<?php if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
        <?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
		<?php } else { ?>  
        <img class="attachment-full wp-post-image" src="http://plugin.miclub.com.au/cms/wp-content/uploads/2015/05/Seminole_Golf_Club_-_Seminole_330849.jpg" alt="<?php the_title(); ?>" />
        <?php } ?> 
        <div class="news-text">
            <h2 class="post-title"><a href="<?php the_permalink() ?>" title="<?php printf( esc_attr__('Permalink to %s'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>                
            <div class="post-content" id="post-content_<?php the_ID(); ?>">
                <p><?php the_excerpt(); ?></p>
          </div>
            <small class="post-meta" id="post-meta_<?php the_ID(); ?>">Published on <?php the_time(__('jS F Y')) ?><?php if (current_user_can( 'delete_others_posts' )) {?> <span>|</span> <?php edit_post_link('Edit'); ?><?php } ?></small>
        </div>
    </div>
    <?php endwhile; endif; wp_reset_query(); ?>
    
    </div>
 </div>

<?php include ('sidebar-members.php'); ?>
<?php get_footer(); ?>   

<script>
	var resizeId;
	jQuery(window).resize(function() {
		clearTimeout(resizeId);
		resizeId = setTimeout(doneResizing, 500);
	});
	function doneResizing(){
    	if (jQuery(window).width() < 641) {
			jQuery('div#right-column').insertBefore('div#left-column');
		} else {
			jQuery('div#right-column').insertAfter('div#left-column');
		}
		console.log('resize');
	}
	doneResizing();
</script>