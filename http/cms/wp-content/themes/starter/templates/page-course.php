<?php 
/* Template Name: Course Tour */
get_header(); ?>

<div id="left-column">
	<h1><?php the_title(); ?></h1>
	<ul id="holes">
		<li class="prev nolink">&laquo;</li>
		<?php 
		$count = 1;
		$holeNumbers = new WP_Query(array(
		'course' => get_query_var('course'),
		'posts_per_page' => 18
		));
		$IDOutsideLoop = $post->ID;
		global $post;
		$args = array( 'post_type' => 'course', 'posts_per_page' => 18, 'orderby'=>'date','order'=>'ASC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<li <?php if(is_single() && $IDOutsideLoop == $post->ID) echo 'class="current"' ?>><a href="<?php the_permalink() ?>" ><?php echo $count ?></a></li>
		<?php $count++;  ?>
		<?php endwhile; ?>
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<?php endwhile; ?>
		<?php endif; ?>
		<li class="next"><a href="/cms/course/hole-1/">&raquo;</a></li>
	</ul>
	<?php the_post_thumbnail();?>
	<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php endif; ?>
	<div id="prev-next" class="clearfix">
		<span class="next"><a href="/cms/course/hole-1/">Begin Tour &raquo;</a></span>
	</div>
</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>