<?php 
/* Template Name: Map */
get_header(); ?>     

<div id="left-column">
	<h1><?php the_title(); ?></h1>
	<?php the_post_thumbnail();?>
    <?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php endif; ?>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript">
	function initialize() {
	var myLatlng = new google.maps.LatLng(-34.958908,117.369525);
	var myOptions = {zoom: 13,center: myLatlng,mapTypeId: google.maps.MapTypeId.ROADMAP}
	var map = new google.maps.Map(document.getElementById("map"), myOptions);
	var infowindow = new google.maps.InfoWindow(
	{ content: '<p><strong>Denmark Golf Club<\/strong><br\/>925 South Coast Hwy<\/p>',position: myLatlng
	});
	infowindow.open(map);
	} 
	google.maps.event.addDomListener(window,'load', initialize);
	</script>
	<p>To scroll and zoom the map please use buttons in the top left, or click &amp; drag your mouse in the map.</p>
	<div id="map"></div>
	
</div>
<?php get_sidebar(); ?>



<?php get_footer(); ?>