<?php 
/* Template Name: Home */
get_header(); ?>

<div class="container">
	<h1>Welcome to <?php bloginfo('name'); ?></h1>
    <?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php endif; ?>
</div>

<?php get_footer(); ?>