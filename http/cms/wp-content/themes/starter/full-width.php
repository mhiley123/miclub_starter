<?php get_header(); ?>

<div class="full-width clearfix">
	<h2><?php the_title(); ?></h2>
	<?php the_post_thumbnail();?>
	<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php endif; ?>
</div>



<?php get_footer(); ?>