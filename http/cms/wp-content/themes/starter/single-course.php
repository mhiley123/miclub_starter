<?php get_header(); ?>

<div id="left-column">
	<h1><?php the_title(); ?> </h1>
	<ul id="holes">
		<?php if ( is_single( 'hole-1' ) ) { ?>
		<li class="prev"><a href="/cms/golf/course-layout/">&laquo;</a></li>
		<?php } else { ?>
		<?php previous_post_link('<li class="prev">%link</li>', '&laquo;'); ?>
		<?php } ?>
		<?php 
		$count = 1;
		$holeNumbers = new WP_Query(array(
		'course' => get_query_var('course'),
		'posts_per_page' => 18
		));
		$IDOutsideLoop = $post->ID;
		global $post;
		$args = array( 'post_type' => 'course', 'posts_per_page' => 18, 'orderby'=>'date','order'=>'ASC' );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<li <?php if(is_single() && $IDOutsideLoop == $post->ID) echo 'class="current"' ?>><a href="<?php the_permalink() ?>" ><?php echo $count ?></a></li>
		<?php $count++;  ?>
		<?php endwhile; ?>
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<?php endwhile; ?>
		<?php endif; ?>
		<?php if ( is_single( 'hole-18' ) ) { ?>
		<li class="next nolink">&raquo;</li>
		<?php } else { ?>
		<?php next_post_link('<li class="next">%link</li>', '&raquo;'); ?>
		<?php } ?>
	</ul>
	<?php the_post_thumbnail();?>
	<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php endif; ?>
	<div id="prev-next" class="clearfix">
		<?php if ( is_single( 'hole-1' ) ) { ?>
		<span class="prev"><a href="/cms/golf/course-layout/">&laquo; Previous</a></span>
		<?php } else { ?>
		<span class="prev"><?php previous_post_link('%link', '&laquo; Previous'); ?></span>
		<?php } ?>
		<span class="overview"><a href="/cms/golf/course-layout/">Back to Course Overview</a></span>
		<?php if ( is_single( 'hole-18' ) ) { ?>
		<span class="next nolink">Next &raquo;</span>
		<?php } else { ?>
		<span class="next"><?php next_post_link('%link', 'Next &raquo;'); ?></span>
		<?php } ?>
	</div>
</div>
<div id="right-column">
	<h3><?php the_title(); ?> Details</h3>
	<table class="holeinfoTbl" cellspacing="0">
		<tr>
			<th></th>
			<th>Par</th>
			<th>Distance</th>
		</tr>
		<tr>
			<td>Mens:</td>
			<td><?php echo get_post_meta($post->ID, 'mens_par', true); ?></td>
			<td><?php echo get_post_meta($post->ID, 'mens_distance', true); ?></td>
		</tr>
		<tr>
			<td>Ladies:</td>
			<td><?php echo get_post_meta($post->ID, 'ladies_par', true); ?></td>
			<td><?php echo get_post_meta($post->ID, 'ladies_distance', true); ?></td>
		</tr>

	</table>
</div>



<?php get_footer(); ?>