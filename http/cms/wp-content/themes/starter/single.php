<?php get_header(); ?>

<div id="left-column">
	<div class="content-wp">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<div class="post-meta">
			<h1><?php the_title(); ?></h1>
			<small class="post-data">Published on <?php the_time(__('jS F Y')) ?> in <?php the_category(', ') ?><?php if (current_user_can( 'delete_others_posts' )) {?>  <span>|</span>  <?php edit_post_link('Edit'); ?><?php } ?></small>
		</div>
		<div class="post-content">
			<?php the_content(); ?>
		</div>
		<?php endwhile; ?>
		<?php endif; ?>
		<p id="back"><a href="javascript: history.go(-1)">&larr; Back to previous page</a></p>
	</div>
</div>
<?php include ('sidebar-news.php'); ?>



<?php get_footer(); ?>