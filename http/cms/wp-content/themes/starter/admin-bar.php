<?php

function function_name() {
global $post;
$thePostID = "/cms/wp-admin/post.php?post=".$post->ID."&action=edit";
}

	// If user is less than admin, hides certain areas in Advanced Options
	get_currentuserinfo() ;
	global $user_level;
	if ($user_level > 5) {?>
    <div id="npMenuJQ">
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="contentTb" style="">
			<div class="tabPad">
				<a href="/cms/" class="npJQButton">Home</a>
                <?php if ( is_page( '283' ) ) { ?>
                    <?php 
                    $page_id = 64; 
                    if ( current_user_can( 'edit_post' )) {
                    echo '<a href="/cms/wp-admin/post.php?post=';
                    echo $page_id;
                    echo '&amp;action=edit" class="npJQButton">Edit This Page</a>';
                    }
                    ?>
                <?php } else { ?>
				<a href="<?php echo(get_edit_post_link()); ?>" class="npJQButton">Edit This Page</a>
                <?php } ?>
				<a href="/cms/wp-admin/post-new.php" class="npJQButton">Add New Post</a>
				<a href="/cms/wp-admin/" class="npJQButton">Manage Content</a>
				<a href="/cms/wp-admin/post-new.php?post_type=event" class="npJQButton">Add New Event</a>
				<a href="/security/logout.msp" class="npJQButton">Logout</a>
			</div>
		</div>
	</div>	
	
<link rel="stylesheet" href="/style/productStyle/adminBarJQ.css" type="text/css" media="screen" />

<?php } else {} ?>