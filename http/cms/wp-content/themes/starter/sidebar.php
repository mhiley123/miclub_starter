<div class="left-column">
	<h3><?php echo get_the_title($post->post_parent); ?></h3>
	<?php if ( is_user_logged_in() ) {
		wp_nav_menu(array(
			'theme_location' => 'secondary',
			'menu_class' => 'members-side-menu list-unstyled',
			'walker' => new Clean_Walker_Nav_Sub_Menu_Members()
		));
	} else {
		wp_nav_menu(array(
			'theme_location' => 'primary',
			'menu_class' => 'guests-side-menu list-unstyled',
			'walker' => new Clean_Walker_Nav_Sub_Menu_Guests()
			));
	}
	if (is_page ('events')) {
		dynamic_sidebar( 'Events' );
	} ?>
	
     <div class="updatesidebar">
        <?php echo apply_filters('the_content', get_post_meta($post->ID, WYSIWYG_META_KEY, true)); ?>
    </div>
</div>