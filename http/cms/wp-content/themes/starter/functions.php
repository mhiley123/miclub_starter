<?php
include_once (TEMPLATEPATH . '/../../../mifunctions/misc.php');
include_once (TEMPLATEPATH . '/../../../mifunctions/arrowsSubMenus.php');
include_once (TEMPLATEPATH . '/../../../mifunctions/walkerMenu.php');
include_once (TEMPLATEPATH . '/../../../mifunctions/walkerSideMenu.php');
include_once (TEMPLATEPATH . '/../../../mifunctions/createMenu.php');
include_once (TEMPLATEPATH . '/../../../mifunctions/roleEditor.php');
include_once (TEMPLATEPATH . '/../../../mifunctions/newsExcerpt.php');
require_once ('assets/php/style-selector.php');
require_once('assets/php/sidecontent.php');

// Custom menu which takes cleans out the default wordpress additions (removes the menu-item-id)
require_once ('assets/php/base/clean-walker.php');
require_once ('assets/php/base/clean-walker-side.php');

// After the theme is activated, customise the wordpress login page
include_once ('assets/php/base/wordpresslogin.php');

// Changing excerpt more - only works where excerpt IS hand-crafted
add_filter('get_the_excerpt', 'manual_excerpt_more');
function manual_excerpt_more($excerpt) {
	$excerpt_more = '';
	if( has_excerpt() ) {
		$excerpt_more = ' <a href="'.get_permalink().'" class="read_more_link" rel="nofollow">Read More &raquo;</a>';
	}
	return $excerpt . $excerpt_more;
}

//ProdGen
function dashboard_widget_function() {
echo "<a href='./product-template-generator/'>Links to Product Template Generator page.</a>";
}
function add_dashboard_widgets() {
wp_add_dashboard_widget('dashboard_widget', 'Link to Product Template Generator', 'dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'page-thumbnails' );
//sidebar registration
add_action( 'widgets_init', 'my_register_sidebars' );

function my_register_sidebars() {

	/* Register the 'primary' sidebar. */
	register_sidebar(
		array(
			'id' => 'events',
			'name' => __( 'Events' ),
			'description' => __( 'Coming Events Calendar Display' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);

	/* Repeat register_sidebar() code for additional sidebars. */
}
add_filter( 'wp_feed_cache_transient_lifetime', 
   create_function('$a', 'return 600;') );

// ===================
//custom footer widget
// ===================

function theme_widgets_init() {
    register_sidebar( array(
        'name' => __( 'First Footer Widget' ),
        'id' => 'footer-first',
        'description' => __( 'Appears in the footer' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

}
add_action( 'widgets_init', 'theme_widgets_init' );



// Fallback thumbnail
function get_fbimage() {
  if ((function_exists('has_post_thumbnail')) && (has_post_thumbnail())) {
  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', '' );
  $fbimage = $src[0];
  } else {
    global $post, $posts;
    $fbimage = '';
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i',
    $post->post_content, $matches);
    $fbimage = $matches [1] [0];
  }
  if(empty($fbimage)) {
    $fbimage = print "/cms/wp-content/themes/essential/images/logoThumb.gif";
  } else {
  	return $fbimage;
  }
}

// Thumbnail size for promo links
add_image_size( 'promo-thumbnail', 250, 167, true );

// Image size for pages/posts
/*add_image_size( 'resized-200px', 200, 0, true );*/

// Thumbnail size for footer news
add_image_size( 'footer-thumbnail', 68, 0, true );

// Exclude categories
function wp_categories_filter_pre_get_posts( $query ) {
    if ( is_main_query() ) {
        if ( is_home() || is_archive() ) {
            $query->set( 'cat', '-5' );
        }
    }
    return $query;
}
add_action( 'pre_get_posts', 'wp_categories_filter_pre_get_posts' );

function the_category_filter($thelist,$separator=' ') {
     // list the IDs of the categories to exclude
     $exclude = array(7);
     // create an empty array
     $exclude2 = array();
     
     // loop through the excluded IDs and get their actual names
     foreach($exclude as $c) {
          // store the names in the second array
          $exclude2[] = get_cat_name($c);
     }
     
     // get the list of categories for the current post		
     $cats = explode($separator,$thelist);
     // create another empty array		
     $newlist = array();

     foreach($cats as $cat) {
          // remove the tags from each category
          $catname = trim(strip_tags($cat));

          // check against the excluded categories
          if(!in_array($catname,$exclude2))
          
          // if not in that list, add to the new array
          $newlist[] = $cat;
     }
     // return the new, shortened list
     return implode($separator,$newlist);
}

// add the filter to 'the_category' tag
add_filter('the_category','the_category_filter', 10, 2);

// Course Tour
add_action('init', 'course_register');
function course_register() {
$args = array(
'label' => __('Course Tour'),
'singular_label' => __('Course'),
'public' => true,
'show_ui' => true,
'capability_type' => 'post',
'hierarchical' => false,
'rewrite' => true,
'supports' => array('title', 'editor', 'thumbnail')
);
register_post_type( 'course' , $args );
}
add_action("admin_init", "admin_init");
add_action('save_post', 'save_details');
function admin_init(){
add_meta_box("prodInfo-meta", "Hole Information", "meta_options", "course", "side", "low");
} 
function meta_options(){
global $post;
$custom = get_post_custom($post->ID);
$hole_name = $custom["hole_name"][0];
$white_par = $custom["white_par"][0];
$white_distance = $custom["white_distance"][0];
$yellow_par = $custom["yellow_par"][0];
$yellow_distance = $custom["yellow_distance"][0];
$red_par = $custom["red_par"][0];
$red_distance = $custom["red_distance"][0];
echo '<style type="text/css">#prodInfo-meta th{width:50%;text-align:left;}#prodInfo-meta th+td{width:50%}#prodInfo-meta input{width:100%;float:right;}</style>';
?>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
		 <td><label>Mens Par:</label></td>
		 <td><input name="mens_par" value="<?php echo $mens_par; ?>" /></td>
	 </tr>
	<tr>
		 <td><label>Mens Distance:</label></td>
		 <td><input name="mens_distance" value="<?php echo $mens_distance; ?>" /></td>
	 </tr>
	 <tr>
		 <td><label>Ladies Par:</label></td>
		 <td><input name="ladies_par" value="<?php echo $ladies_par; ?>" /></td>
	 </tr>
	 <tr>
		 <td><label>Ladies Distance:</label></td>
		 <td><input name="ladies_distance" value="<?php echo $ladies_distance; ?>" /></td>
	 </tr>
</table>
<?php
}
add_action('save_post', 'save_details');
function save_details(){
global $post;
update_post_meta($post->ID, "mens_par", $_POST["mens_par"]);
update_post_meta($post->ID, "mens_distance", $_POST["mens_distance"]);
update_post_meta($post->ID, "ladies_par", $_POST["ladies_par"]);
update_post_meta($post->ID, "ladies_distance", $_POST["ladies_distance"]);
}

?>