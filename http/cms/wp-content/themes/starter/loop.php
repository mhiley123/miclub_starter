<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<div <?php post_class('news-item'); ?>>
	<div class="post-meta" id="post-meta_<?php the_ID(); ?>">
		<h2><a href="<?php the_permalink() ?>" title="<?php printf( esc_attr__('Permalink to %s'), the_title_attribute('echo=0') ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>   
		<small class="post-data">Published on <?php the_time(__('jS F Y')) ?> in <?php the_category(', ') ?><?php if (current_user_can( 'delete_others_posts' )) {?> <span>|</span> <?php edit_post_link('Edit'); ?><?php } ?></small>
	</div>
	<div class="post-content" id="post-content_<?php the_ID(); ?>">
		<?php the_excerpt(); ?>
	</div>
</div>
<?php endwhile; ?>
<?php endif; ?>