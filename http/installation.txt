Welcome to the MiClub Installation Guide
----------------------------------------
1) You must have all of the following files locally structured as the http directory:
    - Gruntfile.js
    - package.json
    - node_modules
    - .ftppass
1) Open package.json and edit the following fields:
    - Name, is the name of of the Wordpress theme.
    - Site, the domain that you are working on, i.e "http://miclubrd6.miclub.com.au/"
    - Server, the FTP root folder name, i.e. "redesign6"
2) Make sure that you have Node JS installed and Python 2.7 (if on Windows)
    - Please visit https://nodejs.org/en/download/ for further instructions.
    - On Windows place `C:\Users\your_username\AppData\Roaming\npm` in your PATH.
    - Ensure that no anti-virus or firewall is blocking NodeJS or port 35729
3) Open up Command Prompt or Terminal
4) CD to the website directory.
    - Type `cd <site directory>`, i.e. `cd C:\Users\Dan\Documents\Sites\highend`
5) Change the IP in footer.php to your local IP address, down the bottom.
6) Run `grunt watch`, this will start the Grunt task and begin watching the project.
7) If the task fails try deleting the node_modules folder and run `npm install`
8) Open your website.
9) Change the default images within highend/assets/images/
10) Open the SASS config file highend/assets/sass/_config.scss
11) Make necessary style changes and save.
12) Your browser should now automatically refresh whenever you make a change to any of the SASS files.
13) Once the site is live, all further changes should be made in the blank theme/style.css file.


Updating editor.css
-----------------------------------------
A grunt task has been setup called `grunt editor` that will automatically use the SASS files and apply all of the
necessary changes to editor.css and upload all necessary files. You will need to run this post-production in order
to make sure that the WYSIWYG editor reflects the primary colours within the editor.

To run this task, simply CD to your working director and run the task `grunt editor`.

TinyPNG
-----------------------------------------
If you want to make sure that all of the .jpgs and .png files have been compressed you can download all of the photos
to your local machine and run the task `grunt tinypng` this will scan over the assets/images folder and the
wp-content/uploads folder and minify all photos. You will then need to make sure that you re-upload the photos
back up to the server.